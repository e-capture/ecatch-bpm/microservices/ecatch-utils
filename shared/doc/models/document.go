package doc_documents

import (
	"time"
)

type Document struct {
	ID           string         `json:"id,omitempty" bson:"_id,omitempty"`
	AutoName     string         `json:"auto_name" bson:"auto_name"`
	DocType      string         `json:"doctype" bson:"doctype"`
	OriginalFile string         `json:"original_file" bson:"original_file"`
	UserCreation string         `json:"user_creation" bson:"user_creation_id"`
	UserDelete   string         `json:"user_delete" bson:"user_delete_id"`
	Batch        string         `json:"batch" bson:"batch"`
	Status       int            `json:"status" bson:"status"`
	Versions     []*Version     `json:"versions" bson:"versions"`
	Entities     []*EntityValue `json:"entities" bson:"entities"`
	Locked       bool           `json:"locked" bson:"locked"`
	File         []byte         `json:"file" bson:"file"`
	FileEncode   string         `json:"file_encode" bson:"file_encode"`
	IsEncode     bool           `json:"is_encode" bson:"is_encode"`
	Format       string         `json:"format" bson:"format"`
	NewVersion   int            `json:"new_version" bson:"new_version"`
	Token        string         `json:"token" bson:"token"`
	Project      string         `json:"project" bson:"project"`
	CreatedAt    time.Time      `json:"created_at" bson:"created_at"`
	UpdatedAt    time.Time      `json:"updated_at" bson:"updated_at"`
}

type Version struct {
	ID        string    `json:"id" bson:"_id"`
	Version   int       `json:"version" bson:"version"`
	Pages     []*Page   `json:"pages" bson:"pages"`
	CreatedAt time.Time `json:"created_at" bson:"created_at"`
	UpdatedAt time.Time `json:"updated_at" bson:"updated_at"`
}

type Page struct {
	ID        string    `json:"id" bson:"_id"`
	Path      string    `json:"path" bson:"path"`
	FileName  string    `json:"file_name" bson:"file_name"`
	Storage   string    `json:"storage" bson:"storage"`
	Hash      string    `json:"hash" bson:"hash"`
	CreatedAt time.Time `json:"created_at" bson:"created_at"`
	UpdatedAt time.Time `json:"updated_at" bson:"updated_at"`
}

type EntityValue struct {
	ID         string             `json:"id" bson:"id"`
	Entity     string             `json:"entity" bson:"entity"`
	Attributes []*AttributesValue `json:"attributes" bson:"attributes"`
	CreatedAt  time.Time          `json:"created_at" bson:"created_at"`
	UpdatedAt  time.Time          `json:"updated_at" bson:"updated_at"`
}

type AttributesValue struct {
	Name     string `json:"name" bson:"name"`
	Value    string `json:"value" bson:"value"`
	IsCipher bool   `json:"is_cipher" bson:"is_cipher"`
}
