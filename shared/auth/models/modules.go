package auth_model

import "time"

type Module struct {
	ID          string       `json:"id" bson:"_id"`
	Description string       `json:"description" bson:"description"`
	Class       string       `json:"class" bson:"class"`
	Components  []*Component `json:"components" bson:"components"`
	CreatedAt   time.Time    `json:"created_at" bson:"created_at"`
	UpdatedAt   time.Time    `json:"updated_at" bson:"updated_at"`
}
type Component struct {
	Name     string     `json:"name" bson:"name"`
	URLFront string     `json:"url_front" bson:"url_front"`
	Class    string     `json:"class" bson:"class"`
	Elements []*Element `json:"elements" bson:"elements"`
}
type Element struct {
	Name        string `json:"name" bson:"name"`
	Description string `json:"description" bson:"description"`
	URLBack     string `json:"url_back" bson:"url_back"`
}
