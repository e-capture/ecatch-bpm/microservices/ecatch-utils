package auth_model

import (
	"time"
)

// Status => active = 1, block = 2, disable = 99
type User struct {
	ID                     string             `json:"id" bson:"_id"`
	Name                   string             `json:"name" bson:"name"`
	LastName               string             `json:"last_name" bson:"last_name"`
	Password               string             `json:"password" bson:"password"`
	PasswordConfirm        string             `json:"password_comfirm" bson:"password_comfirm"`
	PasswordOld            string             `json:"password_old" bson:"password_old"`
	PasswordHistory        []*PasswordHistory `json:"password_history" bson:"password_history"`
	Roles                  []*string          `json:"roles" bson:"roles"`
	DocTypes               []*int             `json:"doc_types" bson:"doc_types"`
	LoggedUsers            []*LoggedUsers     `json:"logged_users" bson:"logged_users"`
	EmailNotifications     string             `json:"email_notifications" bson:"email_notifications"`
	IdentificationNumber   string             `json:"identification_number" bson:"identification_number"`
	IdentificationType     string             `json:"identification_type" bson:"identification_type"`
	Status                 int                `json:"status" bson:"status"`
	ClientID               int                `json:"client_id" bson:"client_id"`
	RealIP                 string             `json:"real_ip" bson:"real_ip"`
	HostName               string             `json:"host_name" bson:"host_name"`
	TimeOut                int                `json:"time_out" bson:"time_out"`
	FailedAttempts         int                `json:"failed_attempts" bson:"failed_attempts"`
	LastChangePassword     time.Time          `json:"Last_change_password" bson:"Last_change_password"`
	BlockDate              time.Time          `json:"block_date" bson:"block_date"`
	DisabledDate           time.Time          `json:"disabled_date" bson:"disabled_date"`
	Projects               []*string          `json:"projects" bson:"projects"`
	ChangePassword         bool               `json:"change_password" bson:"change_password"`
	ChangePasswordDaysLeft int                `json:"change_password_days_left" bson:"change_password_days_left"`
	LastLogin              time.Time          `json:"last_login" bson:"last_login"`
	Modules                *[]Module          `json:"modules" bson:"modules"`
	Token                  string             `json:"token" bson:"token"`
	SessionID              string             `json:"session_id" bson:"session_id"`
	Colors                 Color              `json:"colors" bson:"colors"`
	SecurityEntities       []*SecurityEntity  `json:"security_entities" bson:"security_entities"`
	CreatedAt              time.Time          `json:"created_at" bson:"created_at"`
	UpdatedAt              time.Time          `json:"updated_at" bson:"updated_at"`
}

type LoggedUsers struct {
	Event     string    `json:"event" bson:"event"`
	HostName  string    `json:"host_name" bson:"host_name"`
	IpRemote  string    `json:"ip_remote" bson:"ip_remote"`
	CreatedAt time.Time `json:"created_at" bson:"created_at"`
}

type PasswordHistory struct {
	Password  string    `json:"password" bson:"password"`
	CreatedAt time.Time `json:"created_at" bson:"created_at"`
}

type Color struct {
	Primary   string `json:"primary" bson:"primary"`
	Secondary string `json:"secondary" bson:"secondary"`
	Tertiary  string `json:"tertiary" bson:"tertiary"`
}
