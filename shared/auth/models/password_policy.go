package auth_model

type PasswordPolicy struct {
	DaysPassValid        int  `json:"days_pass_valid" bson:"days_pass_valid"`                 // DaysPassValid es la cantidad de días que una contraseña es válida
	MaxLength            int  `json:"max_length" bson:"max_length"`                           // MaxLength es el tamaño máximo de una contraseña
	MinLength            int  `json:"min_length" bson:"min_length"`                           // MinLength es el tamaño mínimo de una contraseña
	StorePassNotRepeated int  `json:"store_pass_not_repeated" bson:"store_pass_not_repeated"` // StorePassNotRepeated cantidad de contraseñas almacenadas que no puede repetir el usuario
	FailedAttempts       int  `json:"failed_attempts" bson:"failed_attempts"`                 // FailedAttempts es la cantidad máxima de intentos fallidos de login
	TimeUnlock           int  `json:"time_unlock" bson:"time_unlock"`                         // TimeUnlock es el tiempo que un usuario estará bloqueado (minutos)
	Alpha                int  `json:"alpha" bson:"alpha"`                                     // Alpha Cantidad de caracteres que debe tener la contraseña
	Digits               int  `json:"digits" bson:"digits"`                                   // Digits Cantidad de números que debe tener la contraseña
	Special              int  `json:"special" bson:"special"`                                 // Special Cantidad de caracteres especiales que debe tener la contraseña
	UpperCase            int  `json:"upper_case" bson:"upper_case"`                           // UpperCase cantidad de mayúsculas que debe tener la contraseña
	LowerCase            int  `json:"lower_case" bson:"lower_case"`                           // LowerCase cantidad de minúsculas que debe tener la contraseña
	Enable               bool `json:"enable" bson:"enable"`                                   // Enable Identifica si la política del Role está habilitada o no
	InactivityTime       int  `json:"inactivity_time" bson:"inactivity_time"`                 // InactivityTime es el tiempo máximo que un usuario puede tener sin haberse logueado al sistema
	Timeout              int  `json:"timeout" bson:"timeout"`                                 // Timeout es el tiempo de inactividad de una sesión activa
}

type ResponsePasswordPolicy struct {
	LoginAllowed           int
	ValidityPassChange     int
	FailedAttempts         int
	TimeUnlock             int
	IsEnablePasswordPolicy bool
	Elements               []*string
	Projects               []*string
	Doctypes               []*int
	BlockUser              bool
	IsDateDisallowed       bool
}
