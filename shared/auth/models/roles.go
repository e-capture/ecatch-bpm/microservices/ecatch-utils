package auth_model

import "time"

type Role struct {
	ID               string            `json:"id" bson:"_id"`
	Description      string            `json:"description" bson:"description"`
	SessionsAllowed  int               `json:"sessions_allowed" bson:"sessions_allowed"`
	DateDisallowed   []*DateDisallowed `json:"date_disallowed" bson:"date_disallowed"`
	PasswordPolicy   *PasswordPolicy   `json:"password_policy" bson:"password_policy"`
	DocTypes         []*int            `json:"doc_types" bson:"doc_types"`
	Elements         []*string         `json:"elements" bson:"elements"`
	Projects         []*string         `json:"projects" bson:"projects"`
	Process          []*string         `json:"process" bson:"process"`
	Status           bool              `json:"status" bson:"status"`
	SecurityEntities []*SecurityEntity `json:"security_entities" bson:"security_entities"`
	RoleAllow        []*string         `json:"role_allow" bson:"role_allow"`
	CreatedAt        time.Time         `json:"created_at" bson:"created_at"`
	UpdatedAt        time.Time         `json:"updated_at" bson:"updated_at"`
}

type DateDisallowed struct {
	Description string    `json:"description" bson:"description"`
	BeginsAt    time.Time `json:"begins_at" bson:"begins_at"`
	EndsAt      time.Time `json:"ends_at" bson:"ends_at"`
}

type SecurityEntity struct {
	ID         string       `json:"id" bson:"id"`
	Attributes []*Attribute `json:"attributes" bson:"attributes"`
}

type Attribute struct {
	ID     string `json:"id" bson:"id"`
	Value  string `json:"value" bson:"value"`
	Enable bool   `json:"enable" bson:"enable"`
}
