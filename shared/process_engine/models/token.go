package process_models

import (
	"time"
)

type Token struct {
	ID                   string    `json:"id,omitempty" bson:"_id,omitempty"`
	User                 string    `json:"user" bson:"user"`
	Document             string    `json:"document" bson:"document"`
	Doctype              string    `json:"doctype" bson:"doctype"`
	AutoName             string    `json:"autoname" bson:"autoname"`
	Process              string    `json:"process" bson:"process"`
	ProcessName          string    `json:"process_name" bson:"process_name"`
	Queue                string    `json:"queue" bson:"queue"`
	Execution            string    `json:"execution" bson:"execution"`
	UsersBalance         []string  `json:"users_balance" bson:"users_balance"`
	IsRemove             bool      `json:"is_remove" bson:"is_remove"`
	IsTransit            bool      `json:"is_transit" bson:"is_transit"`
	NewQueues            []string  `json:"new_queues" bson:"new_queues"`
	InputMessage         bool      `json:"input_message" bson:"input_message"`
	Response             string    `json:"response" bson:"response"`
	IsCompleted          bool      `json:"is_completed" bson:"is_completed"`
	QueueInCompleted     string    `json:"queue_in_completed" bson:"queue_in_completed"`
	ExecutionInCompleted string    `json:"execution_in_completed" bson:"execution_in_completed"`
	RuleInCompleted      int       `json:"rule_in_completed" bson:"rule_in_completed"`
	TimeEstimatedProcess time.Time `json:"time_estimated_process" bson:"time_estimated_process"`
	TimeEstimatedQueue   time.Time `json:"ans_queue" bson:"ans_queue"`
	CreatedAt            time.Time `json:"created_at" bson:"created_at"`
	UpdatedAt            time.Time `json:"updated_at" bson:"updated_at"`
	Priority             int       `json:"priority" bson:"priority"`
	NewPriority          int       `json:"new_priority" bson:"new_priority"`
	AssignedUser         string    `json:"assigned_user" bson:"assigned_user"`
	AssignedDate         time.Time `json:"assigned_date" bson:"assigned_date"`
	ExecutionMain        string    `json:"execution_main" bson:"execution_main"`
}

type Tokens []Token
type AnsQueues []AnsQueue

type AnsQueue struct {
	Queue string    `json:"queue" bson:"queue"`
	Ans   time.Time `json:"ans" bson:"ans"`
}
