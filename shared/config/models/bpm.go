package config_bpm_model

import (
	"time"
)

type Bpm struct {
	ID             interface{} `json:"id" bson:"_id,omitempty"`
	Name           string      `json:"name" bson:"name"`
	Description    string      `json:"description" bson:"description"`
	ProcessRoot    string      `json:"process_root" bson:"process_root"`
	Class          string      `json:"class" bson:"class"`
	Ans            int         `json:"ans" bson:"ans"`
	PercentAlert   int         `json:"percent_alert" bson:"percent_alert"`
	TypeProcess    int         `json:"type_process" bson:"type_process"`
	Status         int         `json:"status" bson:"status"`
	Project        string      `json:"project" bson:"project"`
	DocumentIdBpmn string      `json:"document_id_bpmn" bson:"document_id_bpmn"`
	DocumentIdSvg  string      `json:"document_id_svg" bson:"document_id_svg"`
	DocumentIdAns  string      `json:"document_id_ans" bson:"document_id_ans"`
	Version        int         `json:"version" bson:"version"`
	IsLocked       bool        `json:"is_locked" bson:"is_locked"`
	LockedInfo     string      `json:"locked_info" bson:"locked_info"`
	IsPublished    bool        `json:"is_published" bson:"is_published"`
	UserDeleted    string      `json:"user_deleted" bson:"user_deleted"`
	DocTypes       []*string   `json:"doc_types" bson:"doc_types"`
	ProcessRelated []*string   `json:"process_related" bson:"process_related"`
	Roles          []*string   `json:"roles" bson:"roles"`
	Queues         Queues      `json:"queues" bson:"queues"`
	CreatedAt      time.Time   `json:"created_at" bson:"created_at"`
	UpdatedAt      time.Time   `json:"updated_at" bson:"updated_at"`
	DeletedAt      time.Time   `json:"deleted_at" bson:"deleted_at"`
}

type Bpms []Bpm

type Queues []Queue

type Queue struct {
	Name               string     `json:"name" bson:"name"`
	Sequences          int        `json:"sequences" bson:"sequences"`
	BalanceType        int        `json:"balance_type" bson:"balance_type"`
	Class              string     `json:"class" bson:"class"`
	Ans                int        `json:"ans" bson:"ans"`
	PercentAlert       int        `json:"percent_alert" bson:"percent_alert"`
	Status             int        `json:"status" bson:"status"`
	IDBpmnElement      string     `json:"id_bpmn_element" bson:"id_bpmn_element"`
	Roles              []*string  `json:"roles" bson:"roles"`
	Entities           []*string  `json:"entities" bson:"entities"`
	MustConfirmComment bool       `json:"must_confirm_comment" bson:"must_confirm_comment"`
	Comments           []*string  `json:"Comments" bson:"Comments"`
	Executions         Executions `json:"executions" bson:"executions"`
}

type Executions []Execution

type Execution struct {
	Name   string    `json:"name" bson:"name"`
	Type   int       `json:"type" bson:"type"`
	Roles  []*string `json:"roles" bson:"roles"`
	Timers string    `json:"timers" bson:"timers"`
	Rules  []*Rule   `json:"rules" bson:"rules"`
	Class  string    `json:"class" bson:"class"`
}

type Rule struct {
	ID         int      `json:"id" bson:"id"`
	Name       string   `json:"name" bson:"name"`
	First      int      `json:"first" bson:"first"`
	ChildTrue  int      `json:"child_true" bson:"child_true"`
	ChildFalse int      `json:"child_false" bson:"child_false"`
	Params     []*Param `json:"params" bson:"params"`
	Action     string   `json:"action" bson:"action"`
	ItemtypeID int      `json:"itemtype_id" bson:"itemtype_id"`
}

type Timers []*Timer

type Timer struct {
	Name          string    `json:"name" bson:"name"`
	Frequency     int       `json:"frequency" bson:"frequency"`       //0 = ejecuta una vez al dia, n = ejecuta cada n minutos.
	DayOfWeek     string    `json:"day_of_week" bson:"day_of_week"`   //* todos los dias de la semana, 0,1,2,3,4,5,6 = (Domingo,lunes,martes,..)
	DayOfMonth    string    `json:"day_of_month" bson:"day_of_month"` //* todos los dias del mes, 1,2,3,4,5,6
	BeginAt       time.Time `json:"begin_at" bson:"begin_at"`         //* Hora de inicio para ejecucion en el dia
	EndAt         time.Time `json:"end_at" bson:"end_at"`             //* Hora de finalizacion ejecucion en el dia
	Enabled       bool      `json:"enabled" bson:"enabled"`
	IsNotRunning  bool      `json:"is_not_running" bson:"is_not_running"`
	LastExecution time.Time `json:"last_execution" bson:"last_execution"`
	CreatedAt     time.Time `json:"created_at" bson:"created_at"`
	UpdatedAt     time.Time `json:"updated_at" bson:"updated_at"`
}

type Param struct {
	Name  string `json:"name" bson:"name"`
	Value string `json:"value" bson:"value"`
}
