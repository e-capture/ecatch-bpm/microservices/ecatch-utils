package trace

import (
	"log"
	"testing"
	"time"
)

func TestLogger(t *testing.T) {
	t.Log("inicia testing")
	log.Println("inicia testing")
	for i := 0; i < 10; i++ {
		Error.Print("hola Error")
		Trace.Print("Hola Trace")
		Warning.Print("Hola Warning")
		Info.Print("Hola Info")
		if i%5 == 0 {
			time.Sleep(time.Second * 1)
		}
	}
	t.Log("Proceso terminado")
}
