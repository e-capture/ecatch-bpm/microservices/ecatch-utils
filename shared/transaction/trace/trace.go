package trace

import (
	"fmt"
	"runtime"
)

var (
	Error   LogTrace
	Trace   LogTrace
	Info    LogTrace
	Warning LogTrace
)

func init() {
	Error.name = "ERROR"
	Trace.name = "TRACE"
	Info.name = "INFO"
	Warning.name = "WARNING"
}

type LogTrace struct {
	name     string
	filename string
}

func (t LogTrace) Print(msg ...interface{}) {

	_, filename, codeLine, _ := runtime.Caller(1)
	err := CreateLog(t.name, filename, fmt.Sprintf("%v", msg...), codeLine)
	if err != nil {
		fmt.Println("error create logger_trace: ", err)
	}

	return
}

func (t LogTrace) Printf(msg ...interface{}) {

	_, filename, codeLine, _ := runtime.Caller(1)
	err := CreateLog(t.name, filename, fmt.Sprintln(msg...), codeLine)
	if err != nil {
		fmt.Println("error create logger_trace: ", err)
	}

	return
}
func CreateLog(typeMessage, fileName string, message interface{}, codeLine int) error {
	/*mdl := Model{}
	mdl.TypeMessage = typeMessage
	mdl.FileName = fileName
	mdl.CodeLine = codeLine
	mdl.Message = message
	err := mdl.Create()
	if err != nil{
		fmt.Println("error insert in logger_trace database: ", err)
		return err
	}*/
	// fmt.Printf(typeMessage, fileName, codeLine, message)
	return nil
}
