package db

import (
	"context"
	"database/sql"
	"fmt"
	"strings"
	"sync"

	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-utils/logger"

	_ "github.com/denisenkom/go-mssqldb"
	_ "github.com/go-sql-driver/mysql"
	_ "github.com/lib/pq"
	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-utils/env"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var (
	db   *sql.DB
	dbTx *sql.DB
	once sync.Once
	mg   *mongo.Database
)

func init() {
	once.Do(func() {
		setConnection()
		setTxConnection()
	})
}

func setConnection() {
	var err error
	c := env.FromFile()
	if c.DBConnection == "mongodb" {
		clientOptions := options.Client().ApplyURI(connectionString("data"))
		// Connect to MongoDB
		connection, err := mongo.Connect(context.TODO(), clientOptions)
		if err != nil {
			logger.Error.Printf("no se puede conectar a la base de datos MongoDB: %v", err)
			panic(err)
		}

		// Check the connection
		err = connection.Ping(context.TODO(), nil)
		if err != nil {
			logger.Error.Printf("no se cumple prueba de conexión a la base de datos MongoDB: %v", err)
			panic(err)
		}
		mg = connection.Database(c.DBDatabase)
		return

	}
	db, err = sql.Open(c.DBConnection, connectionString("data"))
	if err != nil {
		logger.Error.Printf("no se puede conectar a la base de datos: %v", err)
		panic(err)
	}
}

func setTxConnection() {
	var err error
	c := env.FromFile()
	dbTx, err = sql.Open(c.DBTxConnection, connectionString("log"))
	if err != nil {
		logger.Error.Printf("no se puede conectar a la base de datos de logs: %v", err)
		panic(err)
	}
}

func GetConnection() *sql.DB {
	return db
}

func GetTxConnection() *sql.DB {
	return dbTx
}

func connectionString(t string) string {
	c := env.FromFile()
	var host, database, username, password, instance string
	var port int
	switch t {
	case "data":
		host = c.DBHost
		database = c.DBDatabase
		username = c.DBUserName
		password = c.DBPassword
		instance = c.DBInstance
		port = c.GetDBPort()
	case "log":
		host = c.DBTxHost
		database = c.DBTxDatabase
		username = c.DBTxUserName
		password = c.DBTxPassword
		instance = c.DBTxInstance
		port = c.GetDBTXPort()
	default:
		logger.Error.Print("El tipo de conexión no correspondea data/logs")
		return ""
	}
	switch strings.ToLower(c.DBConnection) {
	case "postgres":
		return fmt.Sprintf("dbname=%s user=%s password=%s host=%s port=%d sslmode=disable", database, username, password, host, port)
	case "sqlserver":
		return fmt.Sprintf(
			"server=%s\\%s;user id=%s;database=%s;password=%s;port=%d", host, instance, username, database, password, port)
	case "mongodb":
		return fmt.Sprintf(
			"mongodb://%s:%s@%s:%d", username, password, host, port)
	}
	logger.Error.Print("el motor de bases de datos solicitado no está configurado aún")

	return ""
}

// RowScanner utilidad para leer la información de scan de Query y QueryRow
type RowScanner interface {
	Scan(dest ...interface{}) error
}

// ExecAffectingOneRow ejecuta una sentencia (statement),
// esperando una sola fila afectada.
func ExecAffectingOneRow(stmt *sql.Stmt, args ...interface{}) error {
	r, err := stmt.Exec(args...)
	if err != nil {
		return fmt.Errorf("could not execute statement: %v", err)
	}
	rowsAffected, err := r.RowsAffected()
	if err != nil {
		return fmt.Errorf("could not get rows affected: %v", err)
	}
	if rowsAffected != 1 {
		return fmt.Errorf("expected 1 row affected, got %d", rowsAffected)
	}

	return nil
}

// Execute ejecuta una sentencia (statement)
func Execute(stmt *sql.Stmt, args ...interface{}) error {
	_, err := stmt.Exec(args...)
	if err != nil {
		return fmt.Errorf("could not execute statement: %v", err)
	}
	return nil
}

func GetTx() (*sql.Tx, error) {
	conn := GetConnection()
	tx, err := conn.Begin()
	if err != nil {
		logger.Error.Printf("creando la transacción: %v", err)
		return nil, err
	}
	return tx, nil
}

func GetMgConnection() *mongo.Database {
	return mg
}
