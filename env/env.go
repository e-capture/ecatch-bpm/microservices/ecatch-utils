package env

import (
	"log"
	"os"
	"strconv"
	"sync"

	"github.com/joho/godotenv"
)

var (
	// c variable de tipo config que será devuelta
	c    config
	once sync.Once
)

// config estructura de la información de ..env
type config struct {
	// Database connection
	DBConnection string
	DBHost       string
	DBInstance   string
	DBPort       string
	DBDatabase   string
	DBUserName   string
	DBPassword   string

	// Database Log connection
	DBTxConnection string
	DBTxHost       string
	DBTxInstance   string
	DBTxPort       string
	DBTxDatabase   string
	DBTxUserName   string
	DBTxPassword   string

	// General .env for app
	AppServiceName string
	AppPort        string

	// Archivos log
	AppPathLog        string
	LogReviewInterval string
	AppRegisterLog    string

	// GRPC Config Server
	GrpcConfigHost string
	GrpcConfigPort string

	// GRPC DocEngine Server
	GrpcDocEngineHost string
	GrpcDocEnginePort string

	// GRPC Auth Server
	GrpcAuthHost string
	GrpcAuthPort string

	// GRPC ProcessEngine Server
	GrpcProcessEngineHost string
	GrpcProcessEnginePort string

	// GRPC RulesEngine Server
	GrpcRulesEngineHost string
	GrpcRulesEnginePort string

	// GRPC Transactions Server
	GrpcTransactionsHost string
	GrpcTransactionsPort string

	// GRPC Worker Server
	GrpcWorkerEngineHost string
	GrpcWorkerEnginePort string

	// RSA
	AppRSAPrivateKey string
	AppRSAPublicKey  string

	// SMTP connection
	SmtpHost     string
	SmtpPort     string
	SmtpEmail    string
	SmtpPassword string
}

// GetDBPort devuelve en int el valor de DBPort
func (c *config) GetDBPort() int {
	i, _ := strconv.Atoi(c.DBPort)
	return i
}

// GetDBTxPort devuelve en int el valor de DBPort
func (c *config) GetDBTXPort() int {
	i, _ := strconv.Atoi(c.DBTxPort)
	return i
}

// GetAppPort devuelve en int el valor de AppPort
func (c *config) GetAppPort() int {
	i, _ := strconv.Atoi(c.AppPort)
	return i
}

// GetLogReviewInterval retorna el tiempo en segundos en que
// los archivos de log deben ser revisados para generar backup y limpiarlos
func (c *config) GetLogReviewInterval() int {
	i, _ := strconv.Atoi(c.LogReviewInterval)
	return i
}

// FromFile Lee el archivo ..env
// Tiene el patrón de diseño singleton
// Así que leerá una única vez el archivo ..env
func FromFile() config {
	once.Do(func() {
		if err := godotenv.Load(); err != nil {
			log.Fatal("Error loading ..env file")
			panic(err)
		}

		//    ------ DATABASE CONNECTION------
		c.DBConnection = os.Getenv("DB_CONNECTION")
		c.DBHost = os.Getenv("DB_HOST")
		c.DBInstance = os.Getenv("DB_INSTANCE")
		c.DBPort = os.Getenv("DB_PORT")
		c.DBDatabase = os.Getenv("DB_DATABASE")
		c.DBUserName = os.Getenv("DB_USERNAME")
		c.DBPassword = os.Getenv("DB_PASSWORD")

		c.DBTxConnection = os.Getenv("DB_TX_CONNECTION")
		c.DBTxHost = os.Getenv("DB_TX_HOST")
		c.DBTxInstance = os.Getenv("DB_TX_INSTANCE")
		c.DBTxPort = os.Getenv("DB_TX_PORT")
		c.DBTxDatabase = os.Getenv("DB_TX_DATABASE")
		c.DBTxUserName = os.Getenv("DB_TX_USERNAME")
		c.DBTxPassword = os.Getenv("DB_TX_PASSWORD")

		//    ------ GENERAL SERVICE------
		c.AppPort = os.Getenv("APP_PORT")
		c.AppServiceName = os.Getenv("APP_SERVICE_NAME")
		c.LogReviewInterval = os.Getenv("LOG_REVIEW_INTERVAL")
		c.AppPathLog = os.Getenv("APP_PATH_LOG")
		c.AppRegisterLog = os.Getenv("APP_REGISTER_LOG")

		//    ------ PORTS AND HOST GRPC------
		c.GrpcConfigHost = os.Getenv("GRPC_CONFIG_HOST")
		c.GrpcConfigPort = os.Getenv("GRPC_CONFIG_PORT")
		c.GrpcProcessEngineHost = os.Getenv("GRPC_PROCESS_ENGINE_HOST")
		c.GrpcProcessEnginePort = os.Getenv("GRPC_PROCESS_ENGINE_PORT")
		c.GrpcDocEngineHost = os.Getenv("GRPC_DOC_ENGINE_HOST")
		c.GrpcDocEnginePort = os.Getenv("GRPC_DOC_ENGINE_PORT")
		c.GrpcAuthHost = os.Getenv("GRPC_AUTH_HOST")
		c.GrpcAuthPort = os.Getenv("GRPC_AUTH_PORT")
		c.GrpcRulesEngineHost = os.Getenv("GRPC_RULE_ENGINE_HOST")
		c.GrpcRulesEnginePort = os.Getenv("GRPC_RULE_ENGINE_PORT")
		c.GrpcTransactionsHost = os.Getenv("GRPC_TRANSACTIONS_HOST")
		c.GrpcTransactionsPort = os.Getenv("GRPC_TRANSACTIONS_PORT")
		c.GrpcWorkerEngineHost = os.Getenv("GRPC_WORKER_ENGINE_HOST")
		c.GrpcWorkerEnginePort = os.Getenv("GRPC_WORKER_ENGINE_PORT")

		//    ------ **RSA* ------
		c.AppRSAPrivateKey = os.Getenv("APP_RSA_PRIVATE_KEY")
		c.AppRSAPublicKey = os.Getenv("APP_RSA_PUBLIC_KEY")

		//    ------ **SMTP* ------
		c.SmtpHost = os.Getenv("SMTP_HOST")
		c.SmtpPort = os.Getenv("SMTP_PORT")
		c.SmtpEmail = os.Getenv("SMTP_EMAIL")
		c.SmtpPassword = os.Getenv("SMTP_PASSWORD")
	})

	return c
}
