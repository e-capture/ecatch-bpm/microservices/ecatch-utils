package transaction

import (
	"time"

	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-utils/logger"
	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-utils/transaction/process"
)

func RegisterProcess(id, token, document, user, event, description, docType, processID, processName, queue, execution, newQueue, project string, executionType int, beginAt, endAt time.Time) {

	p := process.Model{}
	err := p.Create(id, token, document, user, event, description, docType, processID, processName, queue, execution, newQueue, project, executionType, beginAt, endAt)
	if err != nil {
		logger.Error.Printf("Couldn't RegisterProcessTx: %v", err)
	}
}
