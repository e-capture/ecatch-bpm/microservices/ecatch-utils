package rules

import "time"

type Model struct {
	ID            string    `json:"id" bson:"_id,omitempty"`
	IDExecution   string    `json:"id_execution" bson:"id_execution"`
	Token         string    `json:"token" bson:"token"`
	Document      string    `json:"document" bson:"document"`
	Event         string    `json:"event" bson:"event"`
	Description   string    `json:"description" bson:"description"`
	User          string    `json:"user" bson:"user"`
	DocType       string    `json:"doc_type" bson:"doc_type"`
	Process       string    `json:"process" bson:"process"`
	ProcessName   string    `json:"process_name" bson:"process_name"`
	Queue         string    `json:"queue" bson:"queue"`
	Execution     string    `json:"execution" bson:"execution"`
	ExecutionType int       `json:"activity_type" bson:"activity_type"`
	Rule          string    `json:"rule" bson:"rule"`
	Activity      string    `json:"activity" bson:"activity"`
	Route         string    `json:"route" bson:"route"`
	Response      string    `json:"response" bson:"response"`
	BeginAt       time.Time `json:"begin_at" bson:"begin_at"`
	EndAt         time.Time `json:"end_at" bson:"end_at"`
	CreatedAt     time.Time `json:"created_at" bson:"created_at"`
}

func (m *Model) Create(idExecution, token, document, event, description, user, doctype, process, processName, queue, execution, rule, activity, route string, executionType int, beginAt time.Time, response string) error {
	return s.create(idExecution, token, document, event, description, user, doctype, process, processName, queue, execution, rule, activity, route, executionType, beginAt, response)
}

func (m *Model) getByDocument(documentID string) (*[]Model, error) {
	return s.getByDocument(documentID)
}

func (m *Model) getAll() (*[]Model, error) {
	return s.getAll()
}
