package rules

import (
	"database/sql"
	"time"

	"github.com/google/uuid"
	"github.com/lib/pq"
	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-utils/logger/file_logger"

	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-utils/db"
)

type sqlserver struct{}

func (s sqlserver) create(idExecution, token, document, event, description, user, doctype, process, processName, queue, execution, rule, activity, route string, executionType int, beginAt time.Time, response string) error {
	conn := db.GetTxConnection()
	const sqlInsert = `INSERT INTO tx.rules 
		(id,id_execution, token, document, event, description, [user], doc_type, process,process_name, queue, execution,activity_type, [rule], activity, route,response, begin_at,end_at, created_at) VALUES 
		(@id,@id_execution, @token, @document, @event, @description, @user, @doc_type, @process, @process_name, @queue, @execution, @activity_type, @rule, @activity, @route, @response, @begin_at, getDATE(), getDATE()) `
	stmt, err := conn.Prepare(sqlInsert)
	if err != nil {
		file_logger.Error.Printf("preparando la consulta en Create process: %V", err)
		return err

	}
	defer stmt.Close()

	tokenID, err := uuid.Parse(token)
	if err != nil {
		file_logger.Error.Printf("couldn't convert tokenID to UUID: %v", err)
		return err
	}
	documentID, err := uuid.Parse(document)
	if err != nil {
		file_logger.Error.Printf("couldn't convert documentID to UUID: %v", err)
		return err
	}
	UUIDExecution, err := uuid.Parse(idExecution)
	if err != nil {
		file_logger.Error.Printf("couldn't convert idExecution to UUID: %v", err)
		return err
	}

	err = db.ExecAffectingOneRow(
		stmt,
		sql.Named("id", uuid.New()),
		sql.Named("id_execution", UUIDExecution),
		sql.Named("token", tokenID),
		sql.Named("document", documentID),
		sql.Named("event", event),
		sql.Named("description", description),
		sql.Named("user", user),
		sql.Named("doc_type", doctype),
		sql.Named("process", process),
		sql.Named("process_name", processName),
		sql.Named("queue", queue),
		sql.Named("execution", execution),
		sql.Named("activity_type", executionType),
		sql.Named("rule", rule),
		sql.Named("activity", activity),
		sql.Named("route", route),
		sql.Named("response", response),
		sql.Named("begin_at", beginAt),
	)
	if err != nil {
		file_logger.Error.Printf("ejecutando la consulta en Create process: %v", err)
		return err
	}

	return nil

}

func (s sqlserver) getByDocument(documentID string) (*[]Model, error) {
	conn := db.GetTxConnection()
	const sqlGetByDocumentID = `SELECT convert(nvarchar(50),id) id, convert(nvarchar(50),id_execution) id_execution, convert(nvarchar(50),token) token, convert(nvarchar(50), document) document, event, description, [user], doc_type, process, process_name,queue, execution, activity_type, [rule], activity, route, response, begin_at,end_at, created_at 
	FROM tx.rules WITH (NOLOCK) WHERE document = @document_id`
	ms := make([]Model, 0)

	stmt, err := conn.Prepare(sqlGetByDocumentID)
	if err != nil {
		file_logger.Error.Printf("preparando la consulta en getByDocument process: %v", err)
		return nil, err
	}
	defer stmt.Close()

	documentUUID, err := uuid.Parse(documentID)
	if err != nil {
		file_logger.Error.Printf("couldn't convert documentID to UUID: %v", err)
		return nil, err
	}

	rows, err := stmt.Query(sql.Named("document_id", documentUUID))
	if err != nil {
		file_logger.Error.Printf("ejecutando la consulta en getByDocument process: %v", err)
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		m, err := s.scanRow(rows)
		if err != nil {
			file_logger.Error.Printf("escaneando el registro en process: %v", err)
			return nil, err
		}

		ms = append(ms, *m)
	}

	return &ms, nil

}

func (s sqlserver) getAll() (*[]Model, error) {
	ms := make([]Model, 0)
	conn := db.GetTxConnection()
	const sqlGetAll = `SELECT convert(nvarchar(50),id) id,token, document, event, description, user, doctype, process, queue, execution, rule, activity, route, created_at 
		FROM tx.documents WITH (NOLOCK) order by created_at`

	stmt, err := conn.Prepare(sqlGetAll)
	if err != nil {
		file_logger.Error.Printf("preparando la consulta en sqlGetAll trace process: %v", err)
		return nil, err
	}
	defer stmt.Close()

	rs, err := stmt.Query()
	if err != nil {
		file_logger.Error.Printf("ejecutando la consulta en GetAll trace process: %v", err)
		return nil, err
	}
	defer rs.Close()

	for rs.Next() {
		m, err := s.scanRow(rs)
		if err != nil {
			file_logger.Error.Printf("escaneando el registro en trace process: %v", err)
			return nil, err
		}
		ms = append(ms, *m)
	}
	return &ms, nil

}

func (s sqlserver) scanRow(rs db.RowScanner) (*Model, error) {
	m := &Model{}
	ca := pq.NullTime{}
	ba := pq.NullTime{}
	ea := pq.NullTime{}
	err := rs.Scan(
		&m.ID,
		&m.IDExecution,
		&m.Token,
		&m.Document,
		&m.Event,
		&m.Description,
		&m.User,
		&m.DocType,
		&m.Process,
		&m.ProcessName,
		&m.Queue,
		&m.Execution,
		&m.ExecutionType,
		&m.Rule,
		&m.Activity,
		&m.Route,
		&m.Response,
		&ba,
		&ea,
		&ca,
	)
	if err != nil {
		file_logger.Error.Printf("escaneando el Modelo trace Rules: %v", err)
		return nil, err
	}
	m.BeginAt = ba.Time
	m.EndAt = ea.Time
	m.CreatedAt = ca.Time
	return m, nil
}
