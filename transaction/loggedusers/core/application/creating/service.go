package creating

import (
	"github.com/google/uuid"
	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-transaction/pkg/loggedusers/core/domain"
	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-transaction/pkg/loggedusers/core/ports"
)

type service struct {
	repository ports.LoggedusersRepository
}

func NewService(repository ports.LoggedusersRepository) ports.ServiceLoggedusersCreate {
	return &service{repository: repository}
}

func (s *service) Create(Event, HostName, IPRequest, IPRemote, user string) (string, error) {
	id := uuid.New()
	t := domain.NewTrace(id.String(), Event, HostName, IPRequest, IPRemote, user)
	return s.repository.Save(&t)
}
