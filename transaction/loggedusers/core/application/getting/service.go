package getting

import (
	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-transaction/pkg/loggedusers/core/domain"
	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-transaction/pkg/loggedusers/core/ports"
)

type service struct {
	repository ports.LoggedusersRepository
}

func NewService(repository ports.LoggedusersRepository) ports.ServiceLoggedusersGetting {
	return &service{repository: repository}
}

func (s *service) GetByUser(user string) (*[]domain.Loggedusers, error) {
	return s.repository.GetByUser(user)
}

func (s *service) GetAll() (*[]domain.Loggedusers, error) {
	return s.repository.GetAll()
}
