package domain

type Loggedusers struct {
	ID        string `json:"id" bson:"_id,omitempty"`
	Event     string `json:"event" bson:"event"`
	HostName  string `json:"host_name" bson:"host_name"`
	IPRequest string `json:"ip_request" bson:"ip_request"`
	IPRemote  string `json:"ip_remote" bson:"ip_remote"`
	User      string `json:"user" bson:"user"`
}

func NewTrace(ID, Event, HostName, IPRequest, IPRemote, User string) Loggedusers {
	return Loggedusers{ID: ID,
		Event:     Event,
		HostName:  HostName,
		IPRequest: IPRequest,
		IPRemote:  IPRemote,
		User:      User,
	}
}
