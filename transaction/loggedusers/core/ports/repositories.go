package ports

import (
	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-transaction/pkg/loggedusers/core/domain"
)

type LoggedusersRepository interface {
	Save(new *domain.Loggedusers) (string, error)
	GetByUser(user string) (*[]domain.Loggedusers, error)
	GetAll() (*[]domain.Loggedusers, error)
}
