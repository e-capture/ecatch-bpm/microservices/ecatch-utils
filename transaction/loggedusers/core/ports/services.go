package ports

import (
	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-transaction/pkg/loggedusers/core/domain"
)

type ServiceLoggedusersCreate interface {
	Create(event, hostName, iPRequest, ipRemote, user string) (string, error)
}

type ServiceLoggedusersGetting interface {
	GetByUser(user string) (*[]domain.Loggedusers, error)
	GetAll() (*[]domain.Loggedusers, error)
}
