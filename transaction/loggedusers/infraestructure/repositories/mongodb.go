package repositories

import (
	"context"
	"fmt"

	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-transaction/pkg/loggedusers/core/domain"
	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-utils/db"
	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-utils/logger/file_logger"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const (
	modelCollection = "loggedusers"
)

type mongodbLoggeudsersRepo struct {
	loggedusers *domain.Loggedusers
}

func NewLoggedusersMongodbRepository() mongodbLoggeudsersRepo {
	return mongodbLoggeudsersRepo{}
}

func (mg mongodbLoggeudsersRepo) Save(m *domain.Loggedusers) (string, error) {
	connDB := db.GetMgConnection()

	collection := connDB.Collection(modelCollection)
	r, err := collection.InsertOne(context.TODO(), &m)
	if err != nil {
		file_logger.Error.Printf("creando documento: %v", err)
		return "", err
	}

	return r.InsertedID.(string), nil

}

func (mg mongodbLoggeudsersRepo) GetByUser(id string) (*[]domain.Loggedusers, error) {
	connDB := db.GetMgConnection()
	collection := connDB.Collection(modelCollection)
	oid, err := primitive.ObjectIDFromHex(fmt.Sprintf("%v", id))
	if err != nil {
		file_logger.Error.Printf("ejecutando convert obj: %v", err)
		return nil, err
	}
	filter := bson.D{{"_id", oid}}

	findOptions := options.Find()
	rs, err := collection.Find(context.TODO(), filter, findOptions)

	if err != mongo.ErrNoDocuments {
		if err != nil {
			file_logger.Error.Printf("consultando GetByID config: %v", err)
			return nil, err
		}
	}

	ms, err := mg.scanRow(rs)
	if err != nil {
		file_logger.Error.Printf("ejecutando GetAll config: %v", err)
		return nil, err
	}

	return ms, nil

}

func (mg mongodbLoggeudsersRepo) GetAll() (*[]domain.Loggedusers, error) {
	connDB := db.GetMgConnection()
	collection := connDB.Collection(modelCollection)

	findOptions := options.Find()
	rs, err := collection.Find(context.TODO(), bson.D{{}}, findOptions)
	if err != mongo.ErrNoDocuments {
		if err != nil {
			file_logger.Error.Printf("ejecutando GetAll config: %v", err)
			return nil, err
		}
	}

	ms, err := mg.scanRow(rs)
	if err != nil {
		file_logger.Error.Printf("ejecutando GetAll config: %v", err)
		return nil, err
	}

	return ms, nil

}

func (mg mongodbLoggeudsersRepo) scanRow(rs *mongo.Cursor) (*[]domain.Loggedusers, error) {
	results := []domain.Loggedusers{}
	for rs.Next(context.TODO()) {
		elem := domain.Loggedusers{}
		err := rs.Decode(&elem)
		if err != nil {
			file_logger.Error.Printf("escaneando el modelo config: %v", err)
			return nil, err
		}
		results = append(results, elem)
	}

	err := rs.Err()
	if err != nil {
		file_logger.Error.Printf("validando consistencia en el modelo config: %v", err)
		return nil, err
	}

	rs.Close(context.TODO())

	return &results, nil
}
