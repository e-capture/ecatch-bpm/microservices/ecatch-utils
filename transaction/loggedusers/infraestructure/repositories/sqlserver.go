package repositories

import (
	"database/sql"
	"fmt"

	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-transaction/pkg/loggedusers/core/domain"
	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-utils/logger/file_logger"

	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-utils/db"
)

type sqlserverLoggedusersRepo struct {
	loggedusers *domain.Loggedusers
}

func NewLoggedusersSqlServerRepository() sqlserverLoggedusersRepo {
	return sqlserverLoggedusersRepo{}
}

func (s sqlserverLoggedusersRepo) Save(new *domain.Loggedusers) (string, error) {
	conn := db.GetTxConnection()

	sqlInsert := `INSERT INTO tx.loggedusers (id, event, host_name, ip_request, ip_remote, [user]) VALUES (@id, @event, @host_name, @ip_request, @ip_remote,@user)`
	stmt, err := conn.Prepare(sqlInsert)
	if err != nil {
		file_logger.Error.Printf("preparando la consulta en create loggedusers: %v", err)
		return "", err
	}
	defer stmt.Close()

	err = db.ExecAffectingOneRow(
		stmt,
		sql.Named("id", new.ID),
		sql.Named("event", new.Event),
		sql.Named("host_name", new.HostName),
		sql.Named("ip_request", new.IPRequest),
		sql.Named("ip_remote", new.IPRemote),
		sql.Named("user", new.User),
	)
	if err != nil {
		file_logger.Error.Printf("ejecutando script en create trace: %v", err)
		return "", err
	}
	return new.ID, nil

}

func (s sqlserverLoggedusersRepo) GetByUser(user string) (*[]domain.Loggedusers, error) {
	conn := db.GetTxConnection()
	ms := make([]domain.Loggedusers, 0)
	const sqlGetByUser = `SELECT id, event, host_name, ip_request, ip_remote, [user] FROM tx.loggedusers WITH (NOLOCK) WHERE [user] = @user`

	stmt, err := conn.Prepare(sqlGetByUser)
	if err != nil {
		file_logger.Error.Printf("preparando la consulta en GetByID loggedusers: %v", err)
		return nil, err
	}
	defer stmt.Close()
	rows, err := stmt.Query(sql.Named("user", user))
	if err != nil {

		file_logger.Error.Printf("ejecutando la consulta en GetByUser trace: %v", err)
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {

		m, err := s.scanRow(rows)
		if err != nil {
			file_logger.Error.Printf("escaneando el registro en trace: %v", err)
			return nil, err
		}

		ms = append(ms, *m)
	}

	return &ms, nil

}

func (s sqlserverLoggedusersRepo) GetAll() (*[]domain.Loggedusers, error) {
	conn := db.GetTxConnection()
	ms := make([]domain.Loggedusers, 0)
	const sqlGetAll = `SELECT id, event, host_name, ip_request, ip_remote, [user] FROM tx.loggedusers WITH (NOLOCK)`

	stmt, err := conn.Prepare(sqlGetAll)
	if err != nil {
		file_logger.Error.Printf("preparando la consulta en GetAll loggedusers: %v", err)
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {

		file_logger.Error.Printf("ejecutando la consulta en GetAll trace: %v", err)
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {

		m, err := s.scanRow(rows)
		if err != nil {
			file_logger.Error.Printf("escaneando el registro en trace: %v", err)
			return nil, err
		}

		ms = append(ms, *m)
	}

	return &ms, nil

}

func (s sqlserverLoggedusersRepo) scanRow(rs db.RowScanner) (*domain.Loggedusers, error) {
	m := &domain.Loggedusers{}

	var ID []byte

	err := rs.Scan(
		&ID,
		&m.Event,
		&m.HostName,
		&m.IPRequest,
		&m.IPRemote,
		&m.User,
	)
	if err != nil {
		file_logger.Error.Printf("escaneando el modelo trace: %v", err)
		return nil, err
	}
	b := ID
	b[0], b[1], b[2], b[3] = b[3], b[2], b[1], b[0]
	b[4], b[5] = b[5], b[4]
	b[6], b[7] = b[7], b[6]

	m.ID = fmt.Sprintf("%x-%x-%x-%x-%x", b[:4], b[4:6], b[6:8], b[8:10], b[10:])

	return m, nil
}
