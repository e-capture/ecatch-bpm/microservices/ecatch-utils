package server

import (
	"context"

	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-utils/logger"

	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-utils/msgs"

	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-transaction/pkg/loggedusers/core/ports"
	transaction_proto "gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-utils/shared/transaction/proto"
)

type loggedusersTransactionHandler struct {
	creatingService ports.ServiceLoggedusersCreate
	gettingService  ports.ServiceLoggedusersGetting
}

func (l loggedusersTransactionHandler) Create(ctx context.Context, users *transaction_proto.RequestLoggedUsers) (*transaction_proto.ResponseLoggedUsers, error) {
	res := transaction_proto.ResponseLoggedUsers{Error: true}
	data := transaction_proto.ModelLoggedUser{
		Event:     users.LoggedUser.Event,
		HostName:  users.LoggedUser.HostName,
		IpRequest: users.LoggedUser.IpRequest,
		IpRemote:  users.LoggedUser.IpRemote,
		User:      users.LoggedUser.User,
	}
	insertID, err := l.creatingService.Create(data.Event, data.HostName, data.IpRequest, data.IpRemote, data.User)
	if err != nil {
		logger.Error.Println("Insertando el documento en Create: %v", err)
		res.Code, res.Type, res.Msg = msgs.GetMsg(3)
		return &res, nil
	}
	data.Id = insertID
	res.LoggedUsers = append(res.LoggedUsers, &data)
	res.Error = false
	res.Code, res.Type, res.Msg = msgs.GetMsg(4)
	return &res, nil
}

func (l loggedusersTransactionHandler) GetByUser(ctx context.Context, users *transaction_proto.RequestLoggedUsers) (*transaction_proto.ResponseLoggedUsers, error) {
	res := transaction_proto.ResponseLoggedUsers{Error: true}
	response, err := l.gettingService.GetByUser(users.LoggedUser.User)
	if err != nil {
		logger.Error.Println("consultando el documento en GetByUser: %v", err)
		res.Code, res.Type, res.Msg = msgs.GetMsg(3)
		return &res, nil
	}

	for _, lu := range *response {
		data := transaction_proto.ModelLoggedUser{
			Id:        lu.ID,
			Event:     lu.Event,
			HostName:  lu.HostName,
			IpRequest: lu.IPRequest,
			IpRemote:  lu.IPRemote,
			User:      lu.User,
		}
		res.LoggedUsers = append(res.LoggedUsers, &data)
	}
	res.Error = false
	res.Code, res.Type, res.Msg = msgs.GetMsg(29)
	return &res, nil
}

func (l loggedusersTransactionHandler) GetAll(ctx context.Context, _ *transaction_proto.RequestLoggedUsers) (*transaction_proto.ResponseLoggedUsers, error) {
	res := transaction_proto.ResponseLoggedUsers{Error: true}
	response, err := l.gettingService.GetAll()
	if err != nil {
		logger.Error.Println("consultado el documento en GetAll: %v", err)
		res.Code, res.Type, res.Msg = msgs.GetMsg(3)
		return &res, nil
	}

	for _, lu := range *response {
		data := transaction_proto.ModelLoggedUser{
			Id:        lu.ID,
			Event:     lu.Event,
			HostName:  lu.HostName,
			IpRequest: lu.IPRequest,
			IpRemote:  lu.IPRemote,
			User:      lu.User,
		}
		res.LoggedUsers = append(res.LoggedUsers, &data)
	}
	res.Error = false
	res.Code, res.Type, res.Msg = msgs.GetMsg(29)
	return &res, nil
}

func NewLoggedusersServer(
	cS ports.ServiceLoggedusersCreate,
	gS ports.ServiceLoggedusersGetting,
) transaction_proto.LoggedUsersTransactionServicesServer {
	return &loggedusersTransactionHandler{creatingService: cS, gettingService: gS}
}
