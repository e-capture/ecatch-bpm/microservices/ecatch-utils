package server

import (
	"strings"

	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-transaction/pkg/loggedusers/core/application/creating"
	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-transaction/pkg/loggedusers/core/application/getting"
	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-transaction/pkg/loggedusers/core/ports"
	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-transaction/pkg/loggedusers/infraestructure/repositories"
	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-utils/env"
	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-utils/logger/file_logger"
	transaction_proto "gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-utils/shared/transaction/proto"
	"google.golang.org/grpc"
)

func LoggedusersServerGRPC(srv *grpc.Server) {
	repoLoggedusers := FactoryLoggedusersStorage()
	cratingLoggedusersService := creating.NewService(repoLoggedusers)
	gettingLoggedusersService := getting.NewService(repoLoggedusers)
	transaction_proto.RegisterLoggedUsersTransactionServicesServer(
		srv, NewLoggedusersServer(
			cratingLoggedusersService,
			gettingLoggedusersService,
		),
	)
}
func FactoryLoggedusersStorage() ports.LoggedusersRepository {
	c := env.FromFile()
	switch strings.ToLower(c.DBConnection) {
	case "mongodb":
		return repositories.NewLoggedusersMongodbRepository()
	case "sqlserver":
		return repositories.NewLoggedusersSqlServerRepository()
	case "postgres":
		fallthrough
	case "oracle":
		fallthrough
	default:
		file_logger.Error.Printf("este motor de bd no está configurado aún: %s", c.DBConnection)
	}
	return nil
}
