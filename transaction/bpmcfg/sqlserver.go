package bpmcfg

import (
	"database/sql"

	"github.com/google/uuid"
	"github.com/lib/pq"
	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-utils/logger/file_logger"

	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-utils/db"
)

const (
	sqlUpdate = `UPDATE log.traces SET type_message = @type_message, file_name = @file_name, code_line = @code_line, message = @message,  updated_at = getdate() WHERE id = @id`
	sqlDelete = `DELETE FROM log.traces WHERE id = @id`
)

type sqlserver struct{}

func (s sqlserver) create(m *Model) error {
	conn := db.GetTxConnection()
	const sqlInsert = `INSERT INTO tx.trace (id,type_message, file_name, code_line, message ) VALUES (@id, @type_message, @file_name, @code_line, @message ) `
	stmt, err := conn.Prepare(sqlInsert)
	if err != nil {
		file_logger.Error.Printf("preparando la consulta en Create trace: %V", err)
		return err

	}
	defer stmt.Close()
	m.ID = uuid.New()
	err = db.ExecAffectingOneRow(
		stmt,
		sql.Named("id", m.ID),
		sql.Named("type_message", m.ID),
	)
	if err != nil {
		file_logger.Error.Printf("ejecutando la consulta en Create trace: %v", err)
		return err
	}

	return nil

}

func (s sqlserver) getByID(m *Model) (*Model, error) {

	conn := db.GetTxConnection()
	const sqlGetByID = `SELECT id, type_message, file_name, code_line, message,  created_at FROM tx.trace WITH (NOLOCK) WHERE id = @id`

	stmt, err := conn.Prepare(sqlGetByID)
	if err != nil {
		file_logger.Error.Printf("preparando la consulta en GetByID trace: %v", err)
		return nil, err
	}
	defer stmt.Close()

	row := stmt.QueryRow(sql.Named("id", m.ID))
	return s.scanRow(row)

}

func (s sqlserver) getAll() (*[]Model, error) {
	conn := db.GetTxConnection()
	const sqlGetAll = `SELECT id, type_message, file_name, code_line, message,  created_at FROM tx.trace WITH (NOLOCK)`
	ms := make([]Model, 0)

	stmt, err := conn.Prepare(sqlGetAll)
	if err != nil {
		file_logger.Error.Printf("preparando la consulta en GetAll trace: %v", err)
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {
		file_logger.Error.Printf("ejecutando la consulta en GetAll trace: %v", err)
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		m, err := s.scanRow(rows)
		if err != nil {
			file_logger.Error.Printf("escaneando el registro en trace: %v", err)
			return nil, err
		}

		ms = append(ms, *m)
	}

	return nil, nil

}

func (s sqlserver) scanRow(rs db.RowScanner) (*Model, error) {
	m := &Model{}
	ca := pq.NullTime{}

	err := rs.Scan(
		&m.ID,

		&ca,
	)
	if err != nil {
		file_logger.Error.Printf("escaneando el modelo trace: %v", err)
		return nil, err
	}

	//m.create() = ca.Time
	return m, nil
}
