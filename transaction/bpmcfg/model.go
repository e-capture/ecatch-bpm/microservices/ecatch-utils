package bpmcfg

type Model struct {
	ID             interface{} `json:"id,omitempty" bson:"_id,omitempty"`
	Action         string      `json:"action" bson:"action"`
	Description    string      `json:"description" bson:"description"`
	Process        string      `json:"process" bson:"process"`
	ProcessName    string      `json:"process_name" bson:"process_name"`
	Queue          string      `json:"queue" bson:"queue"`
	Execution      string      `json:"execution" bson:"execution"`
	Rule           string      `json:"rule" bson:"rule"`
	DocumentIDBpmn string      `json:"document_id_bpmn" bson:"entity_id_bpmn"`
	DocumentIDSvg  string      `json:"document_id_svg" bson:"entity_id_svg"`
	DocumentIDAns  string      `json:"document_id_ans" bson:"entity_id_ans"`
	User           string      `json:"user" bson:"user"`
}

func (m *Model) create() error {
	return s.create(m)
}

func (m *Model) getByID() (*Model, error) {
	return s.getByID(m)
}

func (m *Model) getAll() (*[]Model, error) {
	return s.getAll()
}
