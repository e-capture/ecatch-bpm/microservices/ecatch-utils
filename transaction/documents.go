package transaction

import (
	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-utils/logger"
	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-utils/transaction/documents"
)

func RegisterDoc(document, event, description, project, docType, entity, attribute, value, entityID, user string) {
	doc := documents.Model{}
	//fmt.Println(document, event, description, project, docType, entity, attribute, value, entityID, user)
	err := doc.Create(document, event, description, project, docType, entity, attribute, value, entityID, user)
	if err != nil {
		logger.Error.Printf("no se pudo almacenar CreateTraceTx: %v", err)
	}
}
