package configurations

type Model struct {
	ID          interface{} `json:"id,omitempty" bson:"_id,omitempty"`
	Action      string      `json:"action" bson:"action"`
	Description string      `json:"description" bson:"description"`
	User        string      `json:"user" bson:"user"`
	Module      string      `json:"module" bson:"module"`
}

func (m *Model) create() error {
	return s.create(m)
}

func (m *Model) getByID() (*Model, error) {
	return s.getByID(m)
}

func (m *Model) getAll() (*[]Model, error) {
	return s.getAll()
}
