package transaction

import (
	"time"

	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-utils/logger"
	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-utils/transaction/rules"
)

func RegisterRules(idExecution, token, document, event, description, user, docType, process, ProcessName, queue, execution, rule, activity, route string, executionID int, beginAt time.Time, response string) {

	p := rules.Model{}
	err := p.Create(idExecution, token, document, event, description, user, docType, process, ProcessName, queue, execution, rule, activity, route, executionID, beginAt, response)
	if err != nil {
		logger.Error.Printf("Couldn't RegisterRuleTx: %v", err)
	}

}
