package process

import (
	"database/sql"
	"time"

	"github.com/google/uuid"
	"github.com/lib/pq"
	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-utils/logger/file_logger"

	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-utils/db"
)

type sqlserver struct{}

func (s sqlserver) create(id, token, document, user, event, description, doctype, process, processName, queue, execution, newQueue, project string, executionType int, beginAt, endAt time.Time) error {
	conn := db.GetTxConnection()
	const sqlInsert = `INSERT INTO tx.tokens 
		(id,id_execution,token, document, [user], event, description, doc_type, process, process_name, queue, execution,execution_type, new_queue, project, begin_at, end_at, created_at) VALUES 
		(@id,@id_execution, @token, @document, @user, @event, @description, @doc_type, @process, @process_name, @queue, @execution, @execution_type, @new_queue, @project, @begin_at, @end_at, getDATE()) `
	stmt, err := conn.Prepare(sqlInsert)
	if err != nil {
		file_logger.Error.Printf("preparando la consulta en Create process: %V", err)
		return err

	}
	defer stmt.Close()
	UUID, err := uuid.Parse(id)
	if err != nil {
		file_logger.Error.Printf("couldn't convert id to UUID: %v", err)
		return err
	}

	tokenID, err := uuid.Parse(token)
	if err != nil {
		file_logger.Error.Printf("couldn't convert tokenID to UUID: %v", err)
		return err
	}
	documentID, err := uuid.Parse(document)
	if err != nil {
		file_logger.Error.Printf("couldn't convert documentID to UUID: %v", err)
		return err
	}

	err = db.ExecAffectingOneRow(
		stmt,
		sql.Named("id", uuid.New()),
		sql.Named("id_execution", UUID),
		sql.Named("token", tokenID),
		sql.Named("document", documentID),
		sql.Named("user", user),
		sql.Named("event", event),
		sql.Named("description", description),
		sql.Named("doc_type", doctype),
		sql.Named("process", process),
		sql.Named("process_name", processName),
		sql.Named("queue", queue),
		sql.Named("execution", execution),
		sql.Named("execution_type", executionType),
		sql.Named("new_queue", newQueue),
		sql.Named("project", project),
		sql.Named("begin_at", beginAt),
		sql.Named("end_at", endAt),
	)
	if err != nil {
		file_logger.Error.Printf("ejecutando la consulta en Create process: %v", err)
		return err
	}

	return nil

}

func (s sqlserver) getByDocument(documentID string) (*[]Model, error) {
	conn := db.GetTxConnection()
	const sqlGetByDocumentID = `SELECT convert(nvarchar(50),id) id, convert(nvarchar(50),id_execution) id_execution, convert(nvarchar(50),token) token, convert(nvarchar(50),document) document, [user], event, description, doc_type, process, process_name, queue, execution, execution_type, new_queue, project, begin_at, end_at, created_at FROM tx.tokens WITH (NOLOCK) WHERE document = @document_id`
	ms := make([]Model, 0)

	stmt, err := conn.Prepare(sqlGetByDocumentID)
	if err != nil {
		file_logger.Error.Printf("preparando la consulta en GetAll process: %v", err)
		return nil, err
	}
	defer stmt.Close()

	documentUUID, err := uuid.Parse(documentID)
	if err != nil {
		file_logger.Error.Printf("couldn't convert documentID to UUID: %v", err)
		return nil, err
	}

	rows, err := stmt.Query(sql.Named("document_id", documentUUID))
	if err != nil {
		file_logger.Error.Printf("ejecutando la consulta en GetAll process: %v", err)
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		m, err := s.scanRow(rows)
		if err != nil {
			file_logger.Error.Printf("escaneando el registro en process: %v", err)
			return nil, err
		}

		ms = append(ms, *m)
	}

	return &ms, nil

}

func (s sqlserver) getAll() (*[]Model, error) {
	conn := db.GetTxConnection()
	const sqlGetByDocumentID = `SELECT convert(nvarchar(50),id) id,id_execution, token, document, [user], event, description, doc_type, process, queue, execution, created_at FROM tx.tokens WITH (NOLOCK) WHERE document = @document`
	ms := make([]Model, 0)

	stmt, err := conn.Prepare(sqlGetByDocumentID)
	if err != nil {
		file_logger.Error.Printf("preparando la consulta en GetAll process: %v", err)
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {
		file_logger.Error.Printf("ejecutando la consulta en GetAll process: %v", err)
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		m, err := s.scanRow(rows)
		if err != nil {
			file_logger.Error.Printf("escaneando el registro en process: %v", err)
			return nil, err
		}

		ms = append(ms, *m)
	}

	return &ms, nil

}

func (s sqlserver) scanRow(rs db.RowScanner) (*Model, error) {
	m := &Model{}
	ca := pq.NullTime{}
	ba := pq.NullTime{}
	ea := pq.NullTime{}
	nq := sql.NullString{}
	err := rs.Scan(
		&m.ID,
		&m.IDExecution,
		&m.Token,
		&m.Document,
		&m.User,
		&m.Event,
		&m.Description,
		&m.DocType,
		&m.Process,
		&m.ProcessName,
		&m.Queue,
		&m.Execution,
		&m.ExecutionType,
		&nq,
		&m.Project,
		&ba,
		&ea,
		&ca,
	)
	if err != nil {
		file_logger.Error.Printf("escaneando el Modelo trace Process: %v", err)
		return nil, err
	}
	m.NewQueue = nq.String
	m.BeginAt = ba.Time
	m.EndAt = ea.Time
	m.CreatedAt = ca.Time
	return m, nil
}
