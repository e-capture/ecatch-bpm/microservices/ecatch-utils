package process

import "time"

type Model struct {
	ID            string    `json:"id" bson:"_id,omitempty"`
	IDExecution   string    `json:"id_execution" bson:"id_execution"`
	Token         string    `json:"token" bson:"token"`
	Document      string    `json:"document" bson:"document"`
	User          string    `json:"user" bson:"user"`
	Event         string    `json:"event" bson:"event"`
	Description   string    `json:"description" bson:"description"`
	DocType       string    `json:"doc_type" bson:"doc_type"`
	Process       string    `json:"process" bson:"process"`
	ProcessName   string    `json:"process_name" bson:"process_name"`
	Queue         string    `json:"queue" bson:"queue"`
	Execution     string    `json:"execution" bson:"execution"`
	ExecutionType int       `json:"execution_type" bson:"execution_type"`
	NewQueue      string    `json:"new_queue" bson:"new_queue"`
	Project       string    `json:"project" bson:"project"`
	BeginAt       time.Time `json:"begin_at" bson:"begin_at"`
	EndAt         time.Time `json:"end_at" bson:"end_at"`
	CreatedAt     time.Time `json:"created_at" bson:"created_at"`
}

func (m *Model) Create(id, token, document, user, event, description, doctype, process, processName, queue, execution, newQueue, project string, executionType int, beginAt, endAt time.Time) error {
	return s.create(id, token, document, user, event, description, doctype, process, processName, queue, execution, newQueue, project, executionType, beginAt, endAt)
}

func (m *Model) getByDocument(documentID string) (*[]Model, error) {
	return s.getByDocument(documentID)
}

func (m *Model) getAll() (*[]Model, error) {
	return s.getAll()
}
