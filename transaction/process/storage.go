package process

import (
	"strings"
	"time"

	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-utils/logger/file_logger"

	// Esta importación se debe modificar a los paquetes del proyecto
	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-utils/env"
)

var s Storage

func init() {
	setStorage()
}

// Storage to DAO
type Storage interface {
	create(id, token, document, user, event, description, doctype, process, processName, queue, execution, newQueue, project string, executionType int, beginAt, endAt time.Time) error
	getByDocument(documentID string) (*[]Model, error)
	getAll() (*[]Model, error)
}

func setStorage() {
	c := env.FromFile()
	switch strings.ToLower(c.DBConnection) {
	case "mongodb":
		fallthrough
	case "sqlserver":
		s = sqlserver{}
	case "postgres":
		fallthrough
	case "oracle":
		fallthrough
	default:
		file_logger.Error.Printf("este motor de bd no está configurado aún: %s", c.DBConnection)
	}
}
