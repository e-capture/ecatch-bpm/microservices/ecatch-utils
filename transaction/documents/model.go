package documents

import "time"

type Model struct {
	ID          string    `json:"id" bson:"_id,omitempty"`
	Document    string    `json:"document" bson:"document"`
	Event       string    `json:"event" bson:"event"`
	Description string    `json:"description" bson:"description"`
	Project     string    `json:"project" bson:"project"`
	DocType     string    `json:"doc_type" bson:"doc_type"`
	Entities    string    `json:"entity" bson:"entities"`
	EntityID    string    `json:"entity_id" bson:"entity_id"`
	Attribute   string    `json:"attribute" bson:"attribute"`
	Value       string    `json:"value" bson:"value"`
	User        string    `json:"user" bson:"user"`
	CreatedAt   time.Time `json:"created_at" bson:"created_at"`
}

func (m *Model) Create(document, event, description, project, docType, entity, attribute, value, entityID, user string) error {
	return s.create(document, event, description, project, docType, entity, attribute, value, entityID, user)
}

func (m *Model) getByDocument(DocumentID string) (*[]Model, error) {
	return s.getByDocument(DocumentID)
}

func (m *Model) getAll() (*[]Model, error) {
	return s.getAll()
}
