package documents

import (
	"strings"

	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-utils/logger/file_logger"

	// Esta importación se debe modificar a los paquetes del proyecto
	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-utils/env"
)

var s Storage

func init() {
	setStorage()
}

// Storage to DAO
type Storage interface {
	create(document, event, description, project, docType, entity, attribute, value, entityID, user string) error
	getByDocument(DocumentID string) (*[]Model, error)
	getAll() (*[]Model, error)
}

func setStorage() {
	c := env.FromFile()
	switch strings.ToLower(c.DBConnection) {
	case "mongodb":
		fallthrough
	case "sqlserver":
		s = sqlserver{}
	case "postgres":
		fallthrough
	case "oracle":
		fallthrough
	default:
		file_logger.Error.Printf("este motor de bd no está configurado aún: %s", c.DBConnection)
	}
}
