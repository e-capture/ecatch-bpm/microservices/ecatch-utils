package documents

import (
	"database/sql"

	"github.com/google/uuid"
	"github.com/lib/pq"
	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-utils/logger/file_logger"

	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-utils/db"
)

type sqlserver struct{}

func (s sqlserver) create(document, event, description, project, docType, entity, attribute, value, entityID, user string) error {
	conn := db.GetTxConnection()
	const sqlInsertDocuments = ` INSERT INTO [tx].[documents]
           (id, document, event, description, project, doc_type, entity,attribute, [value], entity_id, [user], created_at) 
	   VALUES  (@id, @document, @event, @description, @project, @doc_type, @entity, @attribute, @value, @entity_id, @user, GetDATE()) `

	stmt, err := conn.Prepare(sqlInsertDocuments)
	if err != nil {
		file_logger.Error.Printf("preparando la consulta en Create sqlInsertDocuments: %V", err)
		return err

	}
	defer stmt.Close()
	ID := uuid.New()

	documentID, err := uuid.Parse(document)
	if err != nil {
		file_logger.Error.Printf("couldn't convert documentID to UUID: %v", err)
		return err
	}

	err = db.ExecAffectingOneRow(
		stmt,
		sql.Named("id", ID),
		sql.Named("document", documentID),
		sql.Named("event", event),
		sql.Named("description", description),
		sql.Named("project", project),
		sql.Named("doc_type", docType),
		sql.Named("entity", entity),
		sql.Named("attribute", attribute),
		sql.Named("value", value),
		sql.Named("entity_id", entityID),
		sql.Named("user", user),
	)
	if err != nil {
		file_logger.Error.Printf("ejecutando la consulta en Create sqlInsertDocuments: %v", err)
		return err
	}

	return nil

}

func (s sqlserver) getByDocument(documentID string) (*[]Model, error) {
	ms := make([]Model, 0)
	conn := db.GetTxConnection()
	const sqlGetByDocument = ` SELECT convert(varchar(50),id) id, convert(varchar(50),document) document, event, description, project, doc_type, entity, convert(varchar(50),entity_id) entity_id, attribute, [value], [user], created_at FROM tx.documents WITH (NOLOCK) WHERE document = @document_id order by created_at`

	stmt, err := conn.Prepare(sqlGetByDocument)
	if err != nil {
		file_logger.Error.Printf("preparando la consulta en getByDocument trace: %v", err)
		return nil, err
	}
	defer stmt.Close()

	documentUUID, err := uuid.Parse(documentID)
	if err != nil {
		file_logger.Error.Printf("couldn't convert documentID to UUID: %v", err)
		return nil, err
	}

	rs, err := stmt.Query(sql.Named("document_id", documentUUID))
	if err != nil {
		file_logger.Error.Printf("ejecutando la consulta en getByDocument trace: %v", err)
		return nil, err
	}
	defer rs.Close()

	for rs.Next() {
		m, err := s.scanRow(rs)
		if err != nil {
			file_logger.Error.Printf("escaneando el registro en getByDocument: %v", err)
			return nil, err
		}

		ms = append(ms, *m)
	}
	return &ms, nil

}

func (s sqlserver) getAll() (*[]Model, error) {
	ms := make([]Model, 0)
	conn := db.GetTxConnection()
	const sqlGetAll = `SELECT id, document, event, description, project, doc_type, entity,attribute, [value], entity_id, [user], created_at 
		FROM tx.documents WITH (NOLOCK) order by created_at`

	stmt, err := conn.Prepare(sqlGetAll)
	if err != nil {
		file_logger.Error.Printf("preparando la consulta en sqlGetAll trace document: %v", err)
		return nil, err
	}
	defer stmt.Close()

	rs, err := stmt.Query()
	if err != nil {
		file_logger.Error.Printf("ejecutando la consulta en GetAll trace document: %v", err)
		return nil, err
	}
	defer rs.Close()

	for rs.Next() {
		m, err := s.scanRow(rs)
		if err != nil {
			file_logger.Error.Printf("escaneando el registro en trace document: %v", err)
			return nil, err
		}
		ms = append(ms, *m)
	}
	return &ms, nil

}

func (s sqlserver) scanRow(rs db.RowScanner) (*Model, error) {
	m := &Model{}
	ca := pq.NullTime{}
	err := rs.Scan(
		&m.ID,
		&m.Document,
		&m.Event,
		&m.Description,
		&m.Project,
		&m.DocType,
		&m.Entities,
		&m.Attribute,
		&m.Value,
		&m.EntityID,
		&m.User,
		&ca,
	)
	if err != nil {
		file_logger.Error.Printf("escaneando el modelo trace document: %v", err)
		return nil, err
	}

	m.CreatedAt = ca.Time
	return m, nil
}
