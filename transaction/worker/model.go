package worker

type Model struct {
	ID          interface{} `json:"id,omitempty" bson:"_id,omitempty"`
	Name        string      `json:"name" bson:"name"`
	Document    string      `json:"document" bson:"document"`
	Token       string      `json:"token" bson:"token"`
	Process     string      `json:"process" bson:"process"`
	ProcessName string      `json:"process_name" bson:"process_name"`
	Queue       string      `json:"queue" bson:"queue"`
	Execution   string      `json:"execution" bson:"execution"`
	Rule        string      `json:"rule" bson:"rule"`
	Description string      `json:"description" bson:"description"`
	WorkerID    int         `json:"worker_id" bson:"worker_id"`
	User        int         `json:"user" bson:"user"`
	Response    interface{} `json:"response" bson:"response"`
}

func (m *Model) create() error {
	return s.create(m)
}

func (m *Model) getByID() (*Model, error) {
	return s.getByID(m)
}

func (m *Model) getAll() (*[]Model, error) {
	return s.getAll()
}
