package msgs

import (
	"context"
	"fmt"

	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-utils/grpc"
	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-utils/logger"
	config_proto "gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-utils/shared/config/proto"
)

func GetMsg(code int32) (int32, string, string) {
	conn := grpc.GetConfigConnection()
	SrvClient := config_proto.NewConfigServicesMessagesClient(conn)
	response, err := SrvClient.GetByCode(context.Background(), &config_proto.RequestMessage{Data: fmt.Sprintf("%d", code)})
	if err != nil {
		logger.Error.Printf("no se pudo obtener el mensaje: %v", err)
		return code, "no se pudo obtener el mensaje", "INFO"
	}
	return code, response.Data.Type, response.Data.Msg
}
