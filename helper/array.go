package helper

import (
	"time"
)

func DeleteElementsRepeatedArrayString(src []*string) []*string {
	var dest []*string
	for i, element := range src {
		if i == 0 {
			dest = append(dest, element)
			continue
		}
		for _, newElement := range dest {
			if element == newElement {
				break
			}
		}
		dest = append(dest, element)

	}
	return dest
}

func DeleteElementsRepeatedArrayInt(src []*int) []*int {
	var dest []*int
	for i, element := range src {
		if i == 0 {
			dest = append(dest, element)
			continue
		}
		for _, newElement := range dest {
			if element == newElement {
				break
			}
		}
		dest = append(dest, element)

	}

	return dest
}

func GetDateMinorArrayDate(src []time.Time) time.Time {
	var minorDate time.Time
	for i, date := range src {
		if i == 0 {
			minorDate = date
			continue
		}
		if !date.After(minorDate) {
			minorDate = date
		}
	}
	return minorDate

}

func GetDateMajorArrayDate(src []time.Time) time.Time {
	var majorDate time.Time
	for i, date := range src {
		if i == 0 {
			majorDate = date
			continue
		}
		if date.After(majorDate) {
			majorDate = date
		}
	}
	return majorDate
}

// TODO ELiminar funcion y dependencias
func GetProjects(projects []*string) []string {
	var result []string
	for _, p := range projects {
		result = append(result, *p)
	}
	return result
}

func GetElements(array []*string) []string {
	var result []string
	for _, p := range array {
		result = append(result, *p)
	}
	return result
}

func GetElementsInt(array []*int) []int {
	var result []int
	for _, p := range array {
		result = append(result, *p)
	}
	return result
}
