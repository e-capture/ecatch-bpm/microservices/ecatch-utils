package helper

import (
	"fmt"
	"time"

	"database/sql"

	"github.com/lib/pq"
)

// TimeToNull devuelve un registro nulo si
// la fecha está en su valor zero
func TimeToNull(t time.Time) pq.NullTime {
	r := pq.NullTime{}
	r.Time = t

	if !t.IsZero() {
		r.Valid = true
	}

	return r
}

// StringToNull devuelve un registro nulo si el string está vacio
func StringToNull(s string) sql.NullString {
	r := sql.NullString{}
	r.String = s
	if r.String != "" {
		r.Valid = true
	}

	return r
}

// Int64ToNull devuelve un registro nulo si el número es 0
func Int64ToNull(i int64) sql.NullInt64 {
	r := sql.NullInt64{}
	r.Int64 = i
	if r.Int64 > 0 {
		r.Valid = true
	}

	return r
}

// ErrorWithNumber permite buscar el código de error en sqlserver
type ErrorWithNumber interface {
	SQLErrorNumber() int32
}

func UniqueIdentifierToString(b []byte) string {

	b[0], b[1], b[2], b[3] = b[3], b[2], b[1], b[0]
	b[4], b[5] = b[5], b[4]
	b[6], b[7] = b[7], b[6]
	return fmt.Sprintf("%x-%x-%x-%x-%x", b[:4], b[4:6], b[6:8], b[8:10], b[10:])
}
