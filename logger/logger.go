package logger

import (
	"context"
	"fmt"
	"runtime"
	"strings"
	"time"

	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-utils/env"
	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-utils/grpc"
	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-utils/logger/file_logger"
	transaction_proto "gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-utils/shared/transaction/proto"
)

type messageTrace struct {
	TypeMessage string      `json:"type_message"`
	FileName    string      `json:"file_name"`
	CodeLine    int         `json:"code_line"`
	User        string      `json:"user"`
	Message     interface{} `json:"message"`
}

var (
	Error   LogTrace
	Trace   LogTrace
	Info    LogTrace
	Warning LogTrace
)

func init() {
	Error.name = "ERROR"
	Trace.name = "TRACE"
	Info.name = "INFO"
	Warning.name = "WARNING"
}

type LogTrace struct {
	name     string
	filename string
}

func (t LogTrace) Print(msg ...interface{}) {

	_, filename, codeLine, _ := runtime.Caller(1)
	err := createLog("usr", t.name, filename, fmt.Sprintf("%v", msg...), codeLine)
	if err != nil {
		file_logger.Error.Printf("error Crear log en PrintF: %v", err)
		file_logger.Error.Printf("%v", msg)
	}

	return
}

func (t LogTrace) Printf(msg ...interface{}) {
	//TODO
	// fmt.Sprintf not math %
	_, filename, codeLine, _ := runtime.Caller(1)
	err := createLog("usr", t.name, filename, fmt.Sprintln(msg...), codeLine)
	if err != nil {
		file_logger.Error.Printf("error Crear log en PrintF: %v", err)
		file_logger.Error.Printf("%v", msg)
	}

	return
}

func (t LogTrace) Println(msg ...interface{}) {

	_, filename, codeLine, _ := runtime.Caller(1)
	err := createLog("usr", t.name, filename, fmt.Sprintln(msg...), codeLine)
	if err != nil {
		file_logger.Error.Printf("error Crear log en PrintF: %v", err)
		file_logger.Error.Printf("%v", msg)
	}

	return
}
func createLog(userName, typeMessage, fileName string, message interface{}, codeLine int) error {

	c := env.FromFile()
	if strings.ToUpper(c.AppRegisterLog) != "T" {
		return nil
	}
	fmt.Println(time.Now(), userName, typeMessage, fileName, message, codeLine)
	msg := transaction_proto.RequestTraceTx_ModelTrace{
		TypeMessage: typeMessage,
		FileName:    fileName,
		Message:     message.(string),
		CodeLine:    int32(codeLine),
		User:        userName,
	}

	conn := grpc.GetTransactionsConnection()
	SrvClient := transaction_proto.NewTraceTransactionServicesClient(conn)

	_, err := SrvClient.Create(context.Background(), &transaction_proto.RequestTraceTx{Trace: &msg})
	if err != nil {
		file_logger.Error.Printf("no se pudo crear Trace en CreateLog: %v", err)
		return err
	}

	return nil

}

func createNewLog(userName, typeMessage, fileName string, message interface{}, codeLine int) error {
	/*
		msgs := messageTrace{
			TypeMessage: typeMessage,
			FileName: fileName,
			Message: message,
			CodeLine: codeLine,
			User: userName,
		}


		req := transaction_proto.ResponseStruct{

		}



		msgsbyte,err := json.Marshal(msgs)
		if err != nil {
			file_logger.Error.Printf("no se pudo realizar Marshal en CreateLogMarshal: %v", err)
			file_logger.Error.Printf( "%v", msgs)
			return err
		}

		conn := grpc.GetTransactionsConnection()
		SrvClient := transaction_proto.NewTraceTransactionServicesClient(conn)



		_, err = SrvClient.CreateStruct (context.Background(), &transaction_proto.Request{Data: req})
		if err != nil {
			file_logger.Error.Printf("no se pudo crear Trace en CreateLogMarshal: %v", err)
			file_logger.Error.Printf( "%v", msgs)
			return err
		}*/

	return nil

}
