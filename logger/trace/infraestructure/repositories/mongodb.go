package repositories

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-transaction/pkg/trace/core/domain"
	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-utils/logger/file_logger"

	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-utils/db"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const (
	modelCollection = "trace"
)

type mongodbTraceRepo struct {
	traces *domain.Trace
}

func NewTraceMongodbRepository() mongodbTraceRepo {
	return mongodbTraceRepo{}
}

func (mg mongodbTraceRepo) Save(m *domain.Trace) (string, error) {
	connDB := db.GetMgConnection()
	m.CreatedAt = time.Now()
	collection := connDB.Collection(modelCollection)
	r, err := collection.InsertOne(context.TODO(), &m)
	if err != nil {
		file_logger.Error.Printf("creando documento: %v", err)
		return "", err
	}

	return r.InsertedID.(string), nil
}

func (mg mongodbTraceRepo) GetByID(id string) (*domain.Trace, error) {
	connDB := db.GetMgConnection()
	collection := connDB.Collection(modelCollection)
	m := domain.Trace{}
	oid, err := primitive.ObjectIDFromHex(fmt.Sprintf("%v", id))
	if err != nil {
		file_logger.Error.Printf("ejecutando convert obj: %v", err)
		return nil, err
	}

	filter := bson.D{{"_id", oid}}

	err = collection.FindOne(context.TODO(), filter).Decode(&m)

	if err != mongo.ErrNoDocuments {
		if err != nil {
			file_logger.Error.Printf("consultando GetByID config: %v", err)
			return nil, err
		}
	}

	return &m, nil
}

func (mg mongodbTraceRepo) GetAll() (*[]domain.Trace, error) {
	connDB := db.GetMgConnection()
	collection := connDB.Collection(modelCollection)

	findOptions := options.Find()
	rs, err := collection.Find(context.TODO(), bson.D{{}}, findOptions)
	if err != mongo.ErrNoDocuments {
		if err != nil {
			file_logger.Error.Printf("ejecutando GetAll config: %v", err)
			return nil, err
		}
	}

	ms, err := mg.scanRow(rs)
	if err != nil {
		file_logger.Error.Printf("ejecutando GetAll config: %v", err)
		return nil, err
	}

	return ms, nil
}

func (mg mongodbTraceRepo) scanRow(rs *mongo.Cursor) (*[]domain.Trace, error) {
	results := []domain.Trace{}
	for rs.Next(context.TODO()) {
		elem := domain.Trace{}
		err := rs.Decode(&elem)
		if err != nil {
			file_logger.Error.Printf("escaneando el modelo config: %v", err)
			return nil, err
		}
		results = append(results, elem)
	}

	err := rs.Err()
	if err != nil {
		file_logger.Error.Printf("validando consistencia en el modelo config: %v", err)
		return nil, err
	}

	rs.Close(context.TODO())

	return &results, nil
}

func (mg mongodbTraceRepo) GetByTypeMessage(typeMessage string) (*[]domain.Trace, error) {
	panic("implement me")
}

func (mg mongodbTraceRepo) DeleteByTypeMessage(typeMessage string) error {
	panic("implement me")
}

func (mg mongodbTraceRepo) Delete() error {
	panic("implement me")
}
