package repositories

import (
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"github.com/lib/pq"
	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-transaction/pkg/trace/core/domain"
	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-utils/logger/file_logger"

	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-utils/db"
)

const (
	sqlUpdate = `UPDATE log.traces SET type_message = @type_message, file_name = @file_name, code_line = @code_line, message = @message,  updated_at = getdate() WHERE id = @id`
	sqlDelete = `DELETE FROM log.traces WHERE id = @id`
)

type sqlserverTraceRepo struct {
	traces *domain.Trace
}

func NewTraceSqlServerRepository() sqlserverTraceRepo {
	return sqlserverTraceRepo{}
}

func (s sqlserverTraceRepo) Save(new *domain.Trace) (string, error) {
	conn := db.GetConnection()

	sqlInsert := `INSERT INTO tx.trace (id, type_message, file_name, code_line, message ) VALUES (@id, @type_message, @file_name, @code_line, @message)`
	stmt, err := conn.Prepare(sqlInsert)
	if err != nil {
		file_logger.Error.Printf("preparando la consulta en create trace: %v", err)
		return "", err
	}
	defer stmt.Close()

	new.ID = uuid.New().String()
	err = db.ExecAffectingOneRow(
		stmt,
		sql.Named("id", new.ID),
		sql.Named("type_message", new.TypeMessage),
		sql.Named("file_name", new.FileName),
		sql.Named("code_line", new.CodeLine),
		sql.Named("message", new.Message),
	)
	if err != nil {
		file_logger.Error.Printf("ejecutando script en create trace: %v", err)
		return "", err
	}
	return new.ID, nil

}

func (s sqlserverTraceRepo) GetByID(id string) (*domain.Trace, error) {

	conn := db.GetConnection()
	const sqlGetByID = `SELECT id, type_message, file_name, code_line, message,  created_at FROM tx.trace WITH (NOLOCK) WHERE id = @id`

	stmt, err := conn.Prepare(sqlGetByID)
	if err != nil {
		file_logger.Error.Printf("preparando la consulta en GetByID trace: %v", err)
		return nil, err
	}
	defer stmt.Close()
	row := stmt.QueryRow(
		sql.Named("id", id),
	)

	return s.scanRow(row)

}

func (s sqlserverTraceRepo) GetAll() (*[]domain.Trace, error) {
	conn := db.GetConnection()

	const sqlGetAll = `SELECT id, type_message, file_name, code_line, message,  created_at FROM tx.trace WITH (NOLOCK)`
	ms := make([]domain.Trace, 0)

	stmt, err := conn.Prepare(sqlGetAll)
	if err != nil {
		file_logger.Error.Printf("preparando la consulta en GetAll trace: %v", err)
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {

		file_logger.Error.Printf("ejecutando la consulta en GetAll trace: %v", err)
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {

		m, err := s.scanRow(rows)
		if err != nil {
			file_logger.Error.Printf("escaneando el registro en trace: %v", err)
			return nil, err
		}

		ms = append(ms, *m)
	}

	return &ms, nil

}

func (s sqlserverTraceRepo) scanRow(rs db.RowScanner) (*domain.Trace, error) {
	m := &domain.Trace{}
	ca := pq.NullTime{}
	var ID []byte

	err := rs.Scan(
		&ID,
		&m.TypeMessage,
		&m.FileName,
		&m.CodeLine,
		&m.Message,
		&ca,
	)
	if err != nil {
		file_logger.Error.Printf("escaneando el modelo trace: %v", err)
		return nil, err
	}
	b := ID
	b[0], b[1], b[2], b[3] = b[3], b[2], b[1], b[0]
	b[4], b[5] = b[5], b[4]
	b[6], b[7] = b[7], b[6]

	m.ID = fmt.Sprintf("%x-%x-%x-%x-%x", b[:4], b[4:6], b[6:8], b[8:10], b[10:])

	m.CreatedAt = ca.Time
	return m, nil
}

func (s sqlserverTraceRepo) GetByTypeMessage(typeMessage string) (*[]domain.Trace, error) {
	conn := db.GetConnection()

	const sqlGetByTypeMessage = `SELECT id, type_message, file_name, code_line, message,  created_at FROM tx.trace WITH (NOLOCK) WHERE type_message = @type_message`
	ms := make([]domain.Trace, 0)

	stmt, err := conn.Prepare(sqlGetByTypeMessage)
	if err != nil {
		file_logger.Error.Printf("prepare GetByTypeMessage trace: %v", err)
		return nil, err
	}
	defer stmt.Close()
	rows, err := stmt.Query(sql.Named("type_message", typeMessage))
	if err != nil {
		file_logger.Error.Printf("ejecutando la consulta en GetByTypeMessage trace: %v", err)
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {

		m, err := s.scanRow(rows)
		if err != nil {
			file_logger.Error.Printf("escaneando el registro en trace: %v", err)
			return nil, err
		}

		ms = append(ms, *m)

	}

	return &ms, nil
}

func (s sqlserverTraceRepo) DeleteByTypeMessage(typeMessage string) error {
	conn := db.GetConnection()
	const sqlDeleteTypeMessage = `DELETE FROM tx.trace WHERE type_message = @type_message`

	stmt, err := conn.Prepare(sqlDeleteTypeMessage)
	if err != nil {
		file_logger.Error.Printf("prepare DeleteByTypeMessage trace: %v", err)
		return err
	}
	defer stmt.Close()

	_, err = stmt.Exec(sql.Named("type_message", typeMessage))
	if err != nil {
		return fmt.Errorf("ejecuntando DeleteByTypeMessage en trace: %v", err)
	}

	return nil

}

func (s sqlserverTraceRepo) Delete() error {
	conn := db.GetConnection()

	const sqlDelete = `DELETE FROM tx.trace`

	stmt, err := conn.Prepare(sqlDelete)
	if err != nil {
		file_logger.Error.Printf("prepare Delete trace: #{err}")
		return err
	}
	defer stmt.Close()

	_, err = stmt.Exec()
	if err != nil {
		file_logger.Error.Printf("ejecutando Delete en trace: %v", err)
		return err
	}

	return nil

}
