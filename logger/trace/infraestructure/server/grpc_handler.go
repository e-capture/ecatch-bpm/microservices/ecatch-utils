package server

import (
	"context"
	"encoding/json"

	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-transaction/pkg/trace/core/ports"

	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-utils/logger/file_logger"

	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-utils/msgs"

	transaction_proto "gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-utils/shared/transaction/proto"
)

// ServerTraceTransaction logger
type traceTransactionHandler struct {
	creatingService ports.ServiceTraceCreate
	gettingService  ports.ServiceTraceGetting
	deletingService ports.ServiceTraceDeleting
}

// NewWishListServer provides WishList gRPC operations
func NewTraceServer(
	cS ports.ServiceTraceCreate,
	gS ports.ServiceTraceGetting,
	dS ports.ServiceTraceDeleting,
) transaction_proto.TraceTransactionServicesServer {
	return &traceTransactionHandler{creatingService: cS, gettingService: gS, deletingService: dS}
}

func (t *traceTransactionHandler) Create(ctx context.Context, request *transaction_proto.RequestTraceTx) (*transaction_proto.Response, error) {
	res := transaction_proto.Response{Error: true}

	insertID, err := t.creatingService.Create(request.Trace.TypeMessage, request.Trace.FileName, request.Trace.User, request.Trace.Message, int(request.Trace.CodeLine))
	if err != nil {
		file_logger.Error.Println("Insertando el documento en Create: %v", err)
		res.Code, res.Type, res.Msg = msgs.GetMsg(3)
		return &res, nil
	}

	res.Data = []byte(insertID)
	res.Error = false
	res.Code, res.Type, res.Msg = msgs.GetMsg(4)
	return &res, nil
}

func (t *traceTransactionHandler) GetByID(ctx context.Context, tx *transaction_proto.RequestTraceTx) (*transaction_proto.Response, error) {
	res := transaction_proto.Response{Error: true}
	data, err := t.gettingService.GetByID(tx.Trace.Id)
	if err != nil {
		file_logger.Error.Println("No se pudo consultar trace en GetByID: %v", err)
		res.Code, res.Type, res.Msg = msgs.GetMsg(22)
		return &res, nil
	}

	dataBytes, err := json.Marshal(data)
	if err != nil {
		file_logger.Error.Println("no se pudo realizar Marshal en GetByID: %v", err)
		res.Code, res.Type, res.Msg = msgs.GetMsg(1)
		return &res, nil
	}
	res.Data = dataBytes
	res.Error = false
	res.Code, res.Type, res.Msg = msgs.GetMsg(29)
	return &res, nil

}

func (t *traceTransactionHandler) GetAll(ctx context.Context, tx *transaction_proto.RequestTraceTx) (*transaction_proto.Response, error) {
	res := transaction_proto.Response{Error: true}
	data, err := t.gettingService.GetAll()
	if err != nil {
		file_logger.Error.Println("no se pudo consultar documento en GetAll: %v", err)
		res.Code, res.Type, res.Msg = msgs.GetMsg(22)
		return &res, nil
	}
	dataBytes, err := json.Marshal(data)
	if err != nil {
		file_logger.Error.Println("no se pudo realizar marshal en GetAll: %v", err)
		res.Code, res.Type, res.Msg = msgs.GetMsg(1)
		return &res, nil
	}
	res.Data = dataBytes
	res.Error = false
	res.Code, res.Type, res.Msg = msgs.GetMsg(29)
	return &res, nil
}

func (t *traceTransactionHandler) CreateStruct(ctx context.Context, tx *transaction_proto.RequestTraceTx) (*transaction_proto.Response, error) {
	panic("implement me")
}

func (t *traceTransactionHandler) Delete(ctx context.Context, tx *transaction_proto.RequestTraceTx) (*transaction_proto.Response, error) {
	res := transaction_proto.Response{Error: true}
	err := t.deletingService.Delete()
	if err != nil {
		file_logger.Error.Println("No se pudo eliminar documentos en Delete trace: %v", err)
		res.Code, res.Type, res.Msg = msgs.GetMsg(20)
		return &res, nil
	}

	res.Error = false
	res.Code, res.Type, res.Msg = msgs.GetMsg(29)
	return &res, nil
}

func (t *traceTransactionHandler) GetByTypeMsg(ctx context.Context, tx *transaction_proto.RequestTraceTx) (*transaction_proto.Response, error) {
	res := transaction_proto.Response{Error: true}
	data, err := t.gettingService.GetByTypeMessage(tx.Trace.TypeMessage)
	if err != nil {
		file_logger.Error.Println("No se pudo consultar en trace GetByTypeMsg: %v", err)
		return &res, err
	}
	dataBytes, err := json.Marshal(data)
	if err != nil {
		file_logger.Error.Println("no se pudo realizar el Marshal en GetByTypeMessage: %v", err)
		return &res, nil
	}
	res.Data = dataBytes
	res.Error = false
	res.Code, res.Type, res.Msg = msgs.GetMsg(29)
	return &res, nil

}

func (t *traceTransactionHandler) DeleteByTypeMsg(ctx context.Context, tx *transaction_proto.RequestTraceTx) (*transaction_proto.Response, error) {
	res := transaction_proto.Response{Error: true}
	err := t.deletingService.DeleteByTypeMessage(tx.Trace.TypeMessage)
	if err != nil {
		file_logger.Error.Println("No se pudo eliminar documentos en DeleteByTypeMsg trace: %v", err)
		res.Code, res.Type, res.Msg = msgs.GetMsg(20)
		return &res, nil
	}

	res.Error = false
	res.Code, res.Type, res.Msg = msgs.GetMsg(29)
	return &res, nil
}
