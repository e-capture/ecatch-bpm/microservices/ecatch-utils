package server

import (
	"strings"

	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-transaction/pkg/trace/core/application/creating"
	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-transaction/pkg/trace/core/application/deleting"
	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-transaction/pkg/trace/core/application/getting"
	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-transaction/pkg/trace/core/ports"
	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-transaction/pkg/trace/infraestructure/repositories"
	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-utils/env"
	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-utils/logger/file_logger"
	transaction_proto "gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-utils/shared/transaction/proto"
	"google.golang.org/grpc"
)

func TraceServerGRPC(srv *grpc.Server) {
	repoTrace := FactoryTraceStorage()
	cratingTraceService := creating.NewService(repoTrace)
	gettingTraceService := getting.NewService(repoTrace)
	deletingTraceService := deleting.NewService(repoTrace)
	transaction_proto.RegisterTraceTransactionServicesServer(
		srv, NewTraceServer(
			cratingTraceService,
			gettingTraceService,
			deletingTraceService,
		),
	)
}

func FactoryTraceStorage() ports.TraceRepository {
	c := env.FromFile()
	switch strings.ToLower(c.DBConnection) {
	case "mongodb":
		return repositories.NewTraceMongodbRepository()
	case "sqlserver":
		return repositories.NewTraceSqlServerRepository()
	case "postgres":
		fallthrough
	case "oracle":
		fallthrough
	default:
		file_logger.Error.Printf("este motor de bd no está configurado aún: %s", c.DBConnection)
	}
	return nil
}
