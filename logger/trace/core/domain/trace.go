package domain

import (
	"time"
)

type Trace struct {
	ID          string    `json:"id" bson:"_id,omitempty"`
	TypeMessage string    `json:"type_message" bson:"type_message"`
	FileName    string    `json:"file_name" bson:"file_name"`
	CodeLine    int       `json:"code_line" bson:"code_line"`
	User        string    `json:"user" bson:"user"`
	Message     string    `json:"message" bson:"message"`
	CreatedAt   time.Time `json:"created_at" bson:"created_at"`
}

func NewTrace(ID, TypeMessage, FileName, User, Message string, CodeLine int) Trace {
	return Trace{ID: ID,
		FileName:    FileName,
		TypeMessage: TypeMessage,
		CodeLine:    CodeLine,
		User:        User,
		Message:     Message,
	}
}
