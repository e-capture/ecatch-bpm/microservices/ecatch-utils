package creating

import (
	"github.com/google/uuid"
	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-transaction/pkg/trace/core/domain"
	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-transaction/pkg/trace/core/ports"
)

type service struct {
	repository ports.TraceRepository
}

func NewService(repository ports.TraceRepository) ports.ServiceTraceCreate {
	return &service{repository: repository}
}

func (s *service) Create(typeMessage, fileName, user, message string, codeLine int) (string, error) {
	id := uuid.New()
	t := domain.NewTrace(id.String(), typeMessage, fileName, user, message, codeLine)
	return s.repository.Save(&t)
}
