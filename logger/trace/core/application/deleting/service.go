package deleting

import (
	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-transaction/pkg/trace/core/ports"
)

type service struct {
	repository ports.TraceRepository
}

func NewService(repository ports.TraceRepository) ports.ServiceTraceDeleting {
	return &service{repository: repository}
}

func (s service) DeleteByTypeMessage(typeMessage string) error {
	return s.repository.DeleteByTypeMessage(typeMessage)
}

func (s service) Delete() error {
	return s.repository.Delete()
}
