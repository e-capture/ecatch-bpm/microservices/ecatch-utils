package getting

import (
	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-transaction/pkg/trace/core/domain"
	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-transaction/pkg/trace/core/ports"
)

type service struct {
	repository ports.TraceRepository
}

func NewService(repository ports.TraceRepository) ports.ServiceTraceGetting {
	return &service{repository: repository}
}

func (s *service) GetByID(id string) (*domain.Trace, error) {
	return s.repository.GetByID(id)
}

func (s *service) GetAll() (*[]domain.Trace, error) {
	return s.repository.GetAll()
}

func (s *service) GetByTypeMessage(typeMessage string) (*[]domain.Trace, error) {
	return s.repository.GetByTypeMessage(typeMessage)
}
