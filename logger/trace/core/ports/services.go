package ports

import "gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-transaction/pkg/trace/core/domain"

type ServiceTraceCreate interface {
	Create(typeMessage, fileName, user, message string, codeLine int) (string, error)
}

type ServiceTraceGetting interface {
	GetByID(id string) (*domain.Trace, error)
	GetAll() (*[]domain.Trace, error)
	GetByTypeMessage(typeMessage string) (*[]domain.Trace, error)
}

type ServiceTraceDeleting interface {
	DeleteByTypeMessage(typeMessage string) error
	Delete() error
}
