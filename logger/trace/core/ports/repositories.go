package ports

import (
	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-transaction/pkg/trace/core/domain"
)

type TraceRepository interface {
	Save(new *domain.Trace) (string, error)
	GetByID(id string) (*domain.Trace, error)
	GetAll() (*[]domain.Trace, error)
	GetByTypeMessage(typeMessage string) (*[]domain.Trace, error)
	DeleteByTypeMessage(typeMessage string) error
	Delete() error
}
