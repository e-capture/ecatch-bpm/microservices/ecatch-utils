package logger

import (
	"log"
	"testing"
)

func TestLogger(t *testing.T) {
	t.Log("inicia testing")
	log.Println("inicia testing")

	for i := 0; i < 10; i++ {
		Error.Print("hola Error")
		Trace.Print("Hola Trace")
		Warning.Print("Hola Warning")
		Info.Print("Hola Info")
	}
	log.Println("finaliza testing")
	t.Log( "Proceso terminado")

}
