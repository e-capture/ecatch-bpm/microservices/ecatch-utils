module gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-utils

go 1.13

require (
	github.com/99designs/gqlgen v0.11.3 // indirect
	github.com/denisenkom/go-mssqldb v0.0.0-20200620013148-b91950f658ec
	github.com/go-sql-driver/mysql v1.5.0
	github.com/golang/protobuf v1.4.0
	github.com/google/uuid v1.1.1
	github.com/joho/godotenv v1.3.0
	github.com/lib/pq v1.7.1
	go.mongodb.org/mongo-driver v1.3.2
	google.golang.org/grpc v1.28.1
	google.golang.org/protobuf v1.21.0
	gopkg.in/ldap.v2 v2.5.1
)
