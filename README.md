# Utils

Componentes de sistema para Ecatch-BPMS


## install grpc:
```bash
$ go get -u github.com/golang/protobuf/{proto,protoc-gen-go}
$ vim ~/.bash_profile
$ Add: "export GO_PATH=~/go" &, "export PATH=$PATH:/$GO_PATH/bin"
$ source ~/.bash_profile
```
## Ejecutar con log.
$ GRPC_GO_LOG_VERBOSITY_LEVEL=99 GRPC_GO_LOG_SEVERITY_LEVEL=info go run server.go ./log/.token.pb.txt
```

## proto gen:
```bash
$ protoc shared/auth/proto/auth.proto --go_out=plugins=grpc:.
$ protoc shared/config/proto/config.proto --go_out=plugins=grpc:.
$ protoc shared/doc/proto/document.proto --go_out=plugins=grpc:.
$ protoc shared/rules_engine/proto/rules_engine.proto --go_out=plugins=grpc:.
$ protoc shared/process_engine/proto/process_engine.proto --go_out=plugins=grpc:.
$ protoc shared/transaction/proto/transaction.proto --go_out=plugins=grpc:.
$ protoc shared/worker/proto/worker.proto --go_out=plugins=grpc:.
```

## Compilación:
```bash
$ go build
```


