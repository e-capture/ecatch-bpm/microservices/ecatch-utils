package grpc

import (
	"sync"

	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-utils/env"
	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-utils/logger/file_logger"

	"google.golang.org/grpc"
)

var (
	once              sync.Once
	connAuth          *grpc.ClientConn
	connConfig        *grpc.ClientConn
	connProcessEngine *grpc.ClientConn
	connRulesEngine   *grpc.ClientConn
	connTransaction   *grpc.ClientConn
	connDocEngine     *grpc.ClientConn
	connWorkerEngine  *grpc.ClientConn
)

func init() {
	once.Do(func() {
		ConnectAuth()
		ConnectConfig()
		ConnectProcessEngine()
		ConnectTransactions()
		ConnectDocEngine()
		ConnectWorkerEngine()
	})
}
func ConnectAuth() {
	c := env.FromFile()
	gRpcConn, err := grpc.Dial(c.GrpcAuthHost+":"+c.GrpcAuthPort, grpc.WithInsecure())
	if err != nil {
		file_logger.Error.Printf("no se pudo conectar a grpc server en connectGRPCServer: %v", err)
		panic(err)
	}
	connAuth = gRpcConn
}
func ConnectConfig() {
	c := env.FromFile()
	gRpcConn, err := grpc.Dial(c.GrpcConfigHost+":"+c.GrpcConfigPort, grpc.WithInsecure())
	if err != nil {
		file_logger.Error.Printf("no se pudo conectar a grpc server en connectGRPCServer: %v", err)
		panic(err)
	}
	connConfig = gRpcConn
}
func ConnectDocEngine() {
	c := env.FromFile()
	gRpcConn, err := grpc.Dial(c.GrpcDocEngineHost+":"+c.GrpcDocEnginePort, grpc.WithInsecure())
	if err != nil {
		file_logger.Error.Printf("no se pudo conectar a grpc server en connectGRPCServer: %v", err)
		panic(err)
	}
	connDocEngine = gRpcConn
}
func ConnectProcessEngine() {
	c := env.FromFile()
	gRpcConn, err := grpc.Dial(c.GrpcProcessEngineHost+":"+c.GrpcProcessEnginePort, grpc.WithInsecure())
	if err != nil {
		file_logger.Error.Printf("no se pudo conectar a grpc server en connectGRPCServer: %v", err)
		panic(err)
	}
	connProcessEngine = gRpcConn
}

func ConnectTransactions() {
	c := env.FromFile()
	gRpcConn, err := grpc.Dial(c.GrpcTransactionsHost+":"+c.GrpcTransactionsPort, grpc.WithInsecure())
	if err != nil {
		file_logger.Error.Printf("no se pudo conectar a grpc server en connectGRPCServer: %v", err)
		panic(err)
	}
	connTransaction = gRpcConn
}

func ConnectWorkerEngine() {
	c := env.FromFile()
	gRpcConn, err := grpc.Dial(c.GrpcWorkerEngineHost+":"+c.GrpcWorkerEnginePort, grpc.WithInsecure())
	if err != nil {
		file_logger.Error.Printf("no se pudo conectar a grpc server en connectGRPCServer: %v", err)
		panic(err)
	}
	connWorkerEngine = gRpcConn
}

//----------------------Export connection ----------------------------

func GetAuthConnection() *grpc.ClientConn {
	return connAuth
}

func GetConfigConnection() *grpc.ClientConn {
	return connConfig
}

func GetProcessEngineConnection() *grpc.ClientConn {
	return connProcessEngine
}

func GetTransactionsConnection() *grpc.ClientConn {
	return connTransaction
}

func GetDocEngineConnection() *grpc.ClientConn {
	return connDocEngine
}

func GetWorkerEngineConnection() *grpc.ClientConn {
	return connWorkerEngine
}
