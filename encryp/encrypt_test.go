package encryp

import (
	"fmt"
	"testing"
)

func TestEncryptDecrypt(t *testing.T) {
	phrase := "Este es el texto que quiero cifrar"
	key := "esta es una clave que debe ser privada"

	e, err := EncryptGCM(phrase, key)
	if err != nil {
		t.Errorf("error al tratar de encriptar: %v", err)
	}
	fmt.Printf("el texto cifrado es: %s\n", e)

	db, err := DecryptGCM(e, key)
	if err != nil {
		t.Errorf("error al tratar de desencriptar: %v", err)
	}

	if phrase != string(db) {
		t.Errorf("hay diferencia entre el texto a cifrar y el descifrado: %s != %s", phrase, db)
	}
}
