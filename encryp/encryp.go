package encryp

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/base64"
	"fmt"
	"io"
	"log"
	"os"

	"gitlab.com/e-capture/ecatch-bpm/microservices/ecatch-utils/logger"

	"errors"

	"crypto/md5"

	"github.com/joho/godotenv"
)

var Keystring string

func init() {
	if err := godotenv.Load(); err != nil {
		panic(err)
		log.Fatal("Error loading ..env file")
	}
	Keystring = os.Getenv("APP_KEY")
}

// encrypt string to base64 crypto using AES
func Encrypt(text string) string {
	key := []byte(Keystring)
	plaintext := []byte(text)

	block, err := aes.NewCipher(key)
	if err != nil {
		panic(err)
	}

	// The IV needs to be unique, but not secure. Therefore it's common to
	// include it at the beginning of the ciphertext.
	ciphertext := make([]byte, aes.BlockSize+len(plaintext))
	iv := ciphertext[:aes.BlockSize]
	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		panic(err)
	}

	stream := cipher.NewCFBEncrypter(block, iv)
	stream.XORKeyStream(ciphertext[aes.BlockSize:], plaintext)

	// convert to base64
	return base64.URLEncoding.EncodeToString(ciphertext)
}

// decrypt from base64 to decrypted string
func Decrypt(cryptoText string) string {

	key := []byte(Keystring)
	ciphertext, _ := base64.URLEncoding.DecodeString(cryptoText)
	block, err := aes.NewCipher(key)
	if err != nil {
		panic(err)
	}

	// The IV needs to be unique, but not secure. Therefore it's common to
	// include it at the beginning of the ciphertext.
	if len(ciphertext) < aes.BlockSize {
		panic("ciphertext too short")
	}
	iv := ciphertext[:aes.BlockSize]
	ciphertext = ciphertext[aes.BlockSize:]

	stream := cipher.NewCFBDecrypter(block, iv)

	// XORKeyStream can work in-place if the two arguments are the same.
	stream.XORKeyStream(ciphertext, ciphertext)

	return fmt.Sprintf("%s", ciphertext)
}

// EncryptGCM cifra un contenido, recibe el texto a cifrar y la llave.
func EncryptGCM(plaintext string, keyString string) (string, error) {
	key := to32Bytes(keyString)
	if len(key) != 32 {
		logger.Error.Printf("se intentó cifrar con EncryptGCM un contenido con una clave de un tamaño diferente a 32 bytes")
		return "", errors.New("la clave de cifrado debe ser de 32 bytes")
	}

	c, err := aes.NewCipher(key[:])
	if err != nil {
		logger.Error.Printf("no se pudo crear el cifrado aes.NewCipher en EncryptGCM: %v", err)
		return "", err
	}

	gcm, err := cipher.NewGCM(c)
	if err != nil {
		logger.Error.Printf("no se pudo crear el cifrado cipher.NewGCM en EncryptGCM: %v", err)
		return "", err
	}

	nonce := make([]byte, gcm.NonceSize())
	if _, err = io.ReadFull(rand.Reader, nonce); err != nil {
		logger.Error.Printf("no se pudo leer el contenido de rand.Reader al tratar de cifrar un contenido con EncryptGCM: %v", err)
		return "", err
	}

	return base64.StdEncoding.EncodeToString(gcm.Seal(nonce, nonce, []byte(plaintext), nil)), nil
}

// DecryptGCM decifra un contenido con una clave de 32 bytes
func DecryptGCM(cipherToDecrypt string, keyString string) ([]byte, error) {
	key := to32Bytes(keyString)
	if len(key) != 32 {
		logger.Error.Printf("se intentó cifrar con EncryptGCM un contenido con una clave de un tamaño diferente a 32 bytes")
		return nil, errors.New("la clave de cifrado debe ser de 32 bytes")
	}

	ciphertext, err := base64.StdEncoding.DecodeString(cipherToDecrypt)
	if err != nil {
		logger.Error.Printf("no se pudo hacer decode del texto cifrado a un slice de bytes base64Decode en DecryptGCM: %v", err)
		return nil, err
	}
	c, err := aes.NewCipher(key[:])
	if err != nil {
		logger.Error.Printf("no se pudo crear el cifrado aes.NewCipher en DecryptGCM: %v", err)
		return nil, err
	}

	gcm, err := cipher.NewGCM(c)
	if err != nil {
		logger.Error.Printf("no se pudo crear el cifrado cipher.NewGCM en DencryptGCM: %v", err)
		return nil, err
	}

	nonceSize := gcm.NonceSize()
	if len(ciphertext) < nonceSize {
		return nil, errors.New("ciphertext too short")
	}

	nonce, ciphertext := ciphertext[:nonceSize], ciphertext[nonceSize:]

	return gcm.Open(nil, nonce, ciphertext, nil)
}

func to32Bytes(s string) []byte {
	return []byte(fmt.Sprintf("%x", md5.Sum([]byte(s))))
}
